<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method void pause()
 *
 * @SuppressWarnings(PHPMD)
*/
class UnitTester extends \Codeception\Actor
{
    use _generated\UnitTesterActions;

    public function buildSession(): \Phalcon\Session\Manager
    {
        $manager = new \Phalcon\Session\Manager();
        $manager->setAdapter(new \Codeception\Lib\Connector\Phalcon5\MemorySession());

        return $manager;
    }
}
