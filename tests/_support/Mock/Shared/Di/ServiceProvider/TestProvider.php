<?php

namespace TwistersFury\Phalcon\Shared\Tests\Support\Mock\Shared\Di\ServiceProvider;

use TwistersFury\Phalcon\Shared\Di\ServiceProvider\AbstractServiceProvider;

class TestProvider extends AbstractServiceProvider
{
    protected function registerSomething()
    {
        $this->set(
            "something",
            function() {
                return "else";
            }
        );
    }
}
