<?php

namespace twistersfury\Tests\Support;

use Phalcon\Mvc\Application;

return new Application(new \Phalcon\Di\FactoryDefault());
