<?php

namespace TwistersFury\Phalcon\Shared\Tests\Support\Extension;

use Codeception\Events;
use Codeception\Event\PrintResultEvent;
use Codeception\Extension;
use TwistersFury\Phalcon\Shared\Support\Debug;

class ZephirCoverage extends Extension
{
    public static $events = [
        Events::RESULT_PRINT_AFTER => 'print'
    ];

    public function print(PrintResultEvent $e)
    {
        Debug::flushCoverage();

        `lcov --no-external --capture --quiet --output-file coverage.info --directory /app`;
        `genhtml -q -o report coverage.info`;
    }
}
