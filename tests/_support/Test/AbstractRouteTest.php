<?php

namespace TwistersFury\Phalcon\Shared\Tests\Support\Test;

use Codeception\Test\Unit;
use UnitTester;

abstract class AbstractRouteTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    public function testRoutes()
    {
        foreach ($this->buildTestSubject()->getRoutes() as $route) {
            $details = $route->getPaths();

            $className = $this->getNamespace() . '\\' . $details['controller'] . 'Controller';

            $this->assertTrue(
                method_exists($className, $details['action'] . 'Action'),
                $className . ' Does Not Have Method ' . $details['action'] . 'Action'
            );
        }
    }

    abstract protected function buildTestSubject() : \Phalcon\Mvc\Router\Group;
    abstract protected function getNamespace() : string;
}
