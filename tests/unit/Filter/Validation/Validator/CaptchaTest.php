<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Filter\Validation\Validator;

use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Filter\Validation;
use Phalcon\Http\Request;
use TwistersFury\Phalcon\Shared\Filter\Validation\Validator\Captcha;
use UnitTester;

class CaptchaTest extends Unit
{
    /** @var Captcha */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $di = new Di();
        $di->set('config', new Config(['google' => ['secret' => 'secret']]));
        $di->set(
            'request',
            $this->makeEmpty(
                Request::class,
                [
                    'getClientAddress' => '123'
                ]
            )
        );

        $this->testSubject = $this->getMockBuilder(Captcha::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['verifyCaptcha'])
            ->getMock();

        $this->testSubject->setDI($di);

        $this->testSubject->initialize();
    }

    public function testValidate()
    {
        $mockValidation = $this->makeEmpty(
            Validation::class,
            [
                'getValue' => 'g-recaptcha-response'
            ]
        );

        $this->testSubject->expects($this->once())
            ->method('verifyCaptcha')
            ->willReturn(true);

        $this->assertTrue($this->testSubject->validate($mockValidation, "g-recaptcha-response"));
    }

    public function testValidateFails()
    {
        $mockValidation = $this->makeEmpty(
            Validation::class,
            [
                'getValue' => 'g-recaptcha-response'
            ]
        );

        $this->testSubject->expects($this->once())
            ->method('verifyCaptcha')
            ->willReturn(false);

        $this->assertFalse($this->testSubject->validate($mockValidation, "g-recaptcha-response"));
    }
}
