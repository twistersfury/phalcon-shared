<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit;

use Codeception\Stub;
use Codeception\Test\Unit;
use org\bovigo\vfs\vfsStream;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Di\DiInterface;
use Phalcon\Events\Manager;
use Phalcon\Logger\Logger;
use Phalcon\Mvc\Application;
use TwistersFury\Phalcon\Shared\Kernel\Exception;
use TwistersFury\Phalcon\Shared\Kernel;
use TwistersFury\Phalcon\Shared\Events\Listener\Di as DiListener;
use TwistersFury\Phalcon\Shared\Events\Listener\View;
use UnitTester;
use org\bovigo\vfs\vfsStreamDirectory;

class KernelTest extends Unit
{
    /** @var vfsStreamDirectory */
    private $setupDir = null;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $mockApplication = $this->getMockBuilder(Application::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->setupDir = vfsStream::setup();

        $di = new Di();
        $di->set('config', new Config(['modules' => []]));
        $di->setShared(Application::class, $mockApplication);

        $di->setShared(
            'logger',
            $this->makeEmpty(Logger::class)
        );

        Di::setDefault($di);
    }

    public function testApplication()
    {
        $di = new Di();
        $di->set('config', new Config(['modules' => []]));
        $di->setShared(
            'logger',
            $this->makeEmpty(Logger::class)
        );

        $di->setShared(
            'eventsManager',
            $this->makeEmpty(Manager::class)
        );

        $mockApplication = Stub::makeEmpty(
            $this->getApplicationClass(),
            [
                'getDi' => function () use ($di) {
                    return $di;
                }
            ]
        );

        $di->set($this->getApplicationClass(), $mockApplication);

        $testSubject = $this->buildKernel($di, codecept_data_dir('kernel'));

        $this->assertSame($di->get($this->getApplicationClass()), $testSubject->getApplication());
        $this->assertSame($testSubject, $di->get('kernel'));
    }

    public function testInvalidBasePath()
    {
        $this->tester->expectThrowable(
            new Exception("Base Path Does Not Exist"),
            function () {
                $this->buildKernel(Di::getDefault(), "/does/not/exist");
            }
        );
    }

    public function testInvalidApplicationPath()
    {
        $this->tester->expectThrowable(
            new Exception("Application Path Does Not Exist"),
            function () {
                $this->buildKernel(Di::getDefault(), $this->setupDir->url());
            }
        );
    }

    public function testInvalidConfigPath()
    {
        $this->tester->expectThrowable(
            new Exception("Config Path Does Not Exist"),
            function () {
                $appDirectory = vfsStream::newDirectory("app");
                $this->setupDir->addChild($appDirectory);

                $this->buildKernel(Di::getDefault(), $this->setupDir->url());
            }
        );
    }

    public function testMissingModulesConfig()
    {
        $this->tester->expectThrowable(
            new Exception("'modules' configuration missing from Config"),
            function () {
                $file = vfsStream::newFile('autoload.php');
                $file->setContent('<?php ');

                $vendorDir = vfsStream::newDirectory('vendor');
                $vendorDir->addChild($file);

                $configDir = vfsStream::newDirectory("config");

                $appDir = vfsStream::newDirectory("app");
                $appDir->addChild($configDir);

                $this->setupDir->addChild($vendorDir);
                $this->setupDir->addChild($appDir);

                $di = new Di();
                $di->set('config', new Config([]));

                $testSubject = $this->buildKernel($di, $this->setupDir->url());

                $testSubject->getApplication();
            }
        );
    }

    public function testPaths()
    {
        $configDir = vfsStream::newDirectory("config");

        $appDir = vfsStream::newDirectory("app");
        $appDir->addChild($configDir);

        $this->setupDir->addChild($appDir);

        $mockManager = $this->getMockBuilder(Manager::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['attach'])
            ->getMock();

        $mockManager->expects($this->atLeastOnce())
            ->method('attach')
            ->withConsecutive(
                [
                    'view',
                    $this->isInstanceOf(View::class)
                ],
                [
                    'di',
                    $this->isInstanceOf(DiListener::class)
                ]
            );

        Di::getDefault()->setShared(
            'eventsManager',
            $mockManager
        );

        Di::getDefault()->setShared(
            View::class,
            $this->makeEmpty(View::class)
        );

        Di::getDefault()->setShared(
            DiListener::class,
            $this->makeEmpty(DiListener::class)
        );

        $testSubject = $this->buildKernel(Di::getDefault(), $this->setupDir->url());

        $this->assertEquals($this->setupDir->url(), $testSubject->getBasePath());
        $this->assertEquals($this->setupDir->url() . '/app', $testSubject->getApplicationPath());
        $this->assertEquals($this->setupDir->url() . "/app/config", $testSubject->getConfigPath());
    }

    protected function buildKernel(DiInterface $di, string $basePath): Kernel
    {
        $className = $this->getKernelClass();

        return new $className($di, $basePath);
    }

    protected function getKernelClass(): string
    {
        return Kernel::class;
    }

    protected function getApplicationClass(): string
    {
        return Application::class;
    }
}
