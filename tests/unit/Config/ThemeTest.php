<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Config;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use Phalcon\Di\Di;
use RuntimeException;
use TwistersFury\Phalcon\Shared\Config\Theme;
use \TwistersFury\Phalcon\Shared\Config\Interfaces\Theme as ThemeInterface;
use TwistersFury\Phalcon\Shared\Kernel;
use UnitTester;

class ThemeTest extends Unit
{
    /** @var Theme */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $di = new Di();

        $this->testSubject = new Theme([
            "name" => "theme",
            "basePath" => codecept_data_dir("/themes")
        ]);

        $this->testSubject->setDi($di);
    }

    public function testGetParentNone()
    {
        $this->assertFalse($this->testSubject->hasParent());
        $this->assertNull($this->testSubject->getParent());
    }

    public function testGetParent(): void
    {
        $mockTheme = $this->makeEmpty(Theme::class);

        $this->testSubject->getDi()->set(
            ThemeInterface::class,
            $mockTheme
        );

        $this->testSubject->set("parent", "parent");

        $this->assertSame($mockTheme, $this->testSubject->getParent());
    }

    public function testGetParentThrowsException(): void
    {
        $this->tester->expectThrowable(
            new RuntimeException("Theme ServiceProvider Not Loaded"),
            function () {
                $this->testSubject->set("parent", "parent");

                $this->testSubject->getParent();
            }
        );
    }
}
