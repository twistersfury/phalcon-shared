<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Logger\Formatter;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Logger\Formatter\JsonContext;
use UnitTester;

class JsonContextTest extends Unit
{
    /** @var JsonContext */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new JsonContext();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
