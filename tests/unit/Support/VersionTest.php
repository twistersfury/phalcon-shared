<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Version;
use UnitTester;

class VersionTest extends Unit
{
    /** @var Version */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Version();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
