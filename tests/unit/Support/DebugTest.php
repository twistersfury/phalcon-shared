<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Debug;
use UnitTester;

class DebugTest extends Unit
{
    /** @var Debug */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Debug();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
