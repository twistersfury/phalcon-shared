<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support\Mvc;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Mvc\Module;
use UnitTester;

class ModuleTest extends Unit
{
    /** @var Module */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Module();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
