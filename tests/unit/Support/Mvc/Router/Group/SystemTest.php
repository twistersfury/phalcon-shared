<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support\Mvc\Router\Group;

use TwistersFury\Phalcon\Shared\Support\Mvc\Router\Group\System;
use TwistersFury\Phalcon\Shared\Tests\Support\Test\AbstractRouteTest;

class SystemTest extends AbstractRouteTest
{
    protected function buildTestSubject(): \Phalcon\Mvc\Router\Group
    {
        return new System();
    }

    protected function getNamespace(): string
    {
        return 'TwistersFury\Phalcon\Shared\Support\Mvc\Controller';
    }
}
