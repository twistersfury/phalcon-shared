<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support\Mvc\Router\Group;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Mvc\Router\Group\Assets;
use UnitTester;

class AssetsTest extends Unit
{
    /** @var Assets */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Assets();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
