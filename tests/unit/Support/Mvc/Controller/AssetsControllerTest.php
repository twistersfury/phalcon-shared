<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support\Mvc\Controller;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Mvc\Controller\AssetsController;
use UnitTester;

class AssetsControllerTest extends Unit
{
    /** @var AssetsController */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new AssetsController();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
