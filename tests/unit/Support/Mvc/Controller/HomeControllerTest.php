<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support\Mvc\Controller;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Mvc\Controller\HomeController;
use UnitTester;

class HomeControllerTest extends Unit
{
    /** @var HomeController */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new HomeController();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
