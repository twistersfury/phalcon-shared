<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Support\Mvc\Controller;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Support\Mvc\Controller\SystemController;
use UnitTester;

class SystemControllerTest extends Unit
{
    /** @var SystemController */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new SystemController();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
