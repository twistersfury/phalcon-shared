<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Events\Listener;

use Codeception\Test\Unit;
use Phalcon\Events\Event;
use Phalcon\Events\EventsAwareInterface;
use Phalcon\Events\Manager;
use TwistersFury\Phalcon\Shared\Di\Http;
use TwistersFury\Phalcon\Shared\Events\Listener\Di;
use UnitTester;

class DiTest extends Unit
{
    /** @var Di */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Di();
    }

    public function testValid()
    {
        $mockObject = $this->getMockBuilder(EventsAwareInterface::class)
            ->onlyMethods(['setEventsManager'])
            ->getMockForAbstractClass();

        $mockObject->expects($this->once())
            ->method('setEventsManager')
            ->with($this->isInstanceOf(Manager::class));

        $this->testSubject->afterServiceResolve(
            $this->makeEmpty(Event::class),
            $this->makeEmpty(
                \Phalcon\Di\Di::class,
                [
                    'get' => $this->makeEmpty(Manager::class)
                ]
            ),
            [
                'instance' => $mockObject
            ]
        );
    }

    public function testInvalid()
    {
        $mockObject = $this->getMockBuilder(Http::class)
            ->addMethods(['setEventsManager'])
            ->getMockForAbstractClass();

        $mockObject->expects($this->never())
            ->method('setEventsManager');

        $this->testSubject->afterServiceResolve(
            $this->makeEmpty(Event::class),
            $this->makeEmpty(\Phalcon\Di\Di::class),
            [
                'instance' => $mockObject
            ]
        );
    }

    public function testNonObject()
    {
        $mockObject = $this->getMockBuilder(Http::class)
            ->addMethods(['setEventsManager'])
            ->getMockForAbstractClass();

        $mockObject->expects($this->never())
            ->method('setEventsManager');

        $this->testSubject->afterServiceResolve(
            $this->makeEmpty(Event::class),
            $this->makeEmpty(\Phalcon\Di\Di::class),
            [
                'instance' => 'something'
            ]
        );
    }
}
