<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Events\Listener;

use Codeception\Test\Unit;
use Phalcon\Di\Di;
use Phalcon\Events\Event;
use Phalcon\Logger\Logger;
use Phalcon\Mvc\Dispatcher;
use TwistersFury\Phalcon\Shared\Events\Listener\View;
use TwistersFury\Phalcon\Shared\Kernel;
use UnitTester;

class ViewTest extends Unit
{
    /** @var View */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new View();
    }

    /**
     * @dataProvider _dpDirectories
     */
    public function testDirectories(string $themeName, string $moduleName, string $handlerClass, array $results, array $data = null)
    {
        $di = new Di();
        $di->setShared(
            'kernel',
            $this->makeEmpty(
                Kernel::class,
                [
                    'getThemeName' => $themeName
                ]
            )
        );

        $di->setShared(
            'dispatcher',
            $this->makeEmpty(
                Dispatcher::class,
                [
                    'getModuleName'   => $moduleName,
                    'getHandlerClass' => $handlerClass
                ]
            )
        );

        $this->testSubject->setDi($di);

        $mockView = $this->getMockBuilder(\Phalcon\Mvc\View::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['setViewsDir'])
            ->getMock();

        $mockView->expects($this->once())->method('setViewsDir')
            ->with($results);

        $this->assertTrue(
            $this->testSubject->beforeRender(
                $this->makeEmpty(Event::class),
                $mockView,
                $data
            )
        );
    }

    public function testViewNotFound()
    {
        $mockView = $this->makeEmpty(
            \Phalcon\Mvc\View::class,
            [
                'getActiveRenderPath' => 'some/path',
                'getViewsDir' => [
                    'another/path'
                ]
            ]
        );

        $mockLogger = $this->getMockBuilder(Logger::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['notice'])
            ->getMock();

        $mockLogger->expects($this->once())->method('notice')
            ->with(
                'View Not Found',
                [
                    'path' => 'some/path',
                    'directories' => [
                        'another/path'
                    ],
                    'data' => null
                ]
            );

        $di = new Di();
        $di->setShared('logger', $mockLogger);
        $this->testSubject->setDi($di);

        $this->assertTrue(
            $this->testSubject->notFoundView(
                $this->makeEmpty(Event::class),
                $mockView,
                'some/path'
            )
        );
    }

    public function _dpDirectories()
    {
        yield [
            'default',
            '',
            '',
            [
                '/default/'
            ]
        ];

        yield [
            'default',
            'module',
            '',
            [
                '/default/module/',
                '/default/'
            ]
        ];

        yield [
            'blah',
            'module',
            '',
            [
                '/blah/module/',
                '/blah/',
                '/default/module/',
                '/default/'
            ]
        ];

        yield [
            'blah',
            '',
            '',
            [
                '/blah/',
                '/default/'
            ]
        ];

        yield [
            'alt',
            'alt',
            '',
            [
                '/something/else/',
                '/something/',
                '/default/else/',
                '/default/'
            ],
            [
                'module' => 'else',
                'theme' => 'something'
            ]
        ];

        yield [
            'theme',
            'module',
            'Ident\Plugin\Admin\User\Controllers\UserController',
            [
                '/theme/admin/module/',
                '/theme/admin/',
                '/theme/module/',
                '/theme/',
                '/default/admin/module/',
                '/default/admin/',
                '/default/module/',
                '/default/'
            ]
        ];
    }
}
