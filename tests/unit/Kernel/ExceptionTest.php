<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Kernel;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Kernel\Exception;
use UnitTester;

class ExceptionTest extends Unit
{
    /** @var Exception */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Exception();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
