<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Exceptions;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Exceptions\RecordNotFound;
use UnitTester;

class RecordNotFoundTest extends Unit
{
    /** @var RecordNotFound */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new RecordNotFound
        ();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
