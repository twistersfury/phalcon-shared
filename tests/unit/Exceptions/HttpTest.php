<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Exceptions;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Exceptions\Http;
use UnitTester;

class HttpTest extends Unit
{
    /** @var Http */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Http();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
