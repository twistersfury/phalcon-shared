<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di;

use Codeception\Test\Unit;
use Phalcon\Di\ServiceProviderInterface;
use ReflectionMethod;
use TwistersFury\Phalcon\Shared\Di\Cli;
use UnitTester;

class CliTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    public function testInitialize()
    {
        $mockRegister = $this->getMockBuilder(ServiceProviderInterface::class)
            ->onlyMethods(['register'])
            ->getMockForAbstractClass();

        $mockRegister->method('register')->willReturnSelf();

        $testSubject = $this->getMockBuilder(Cli::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get', 'register'])
            ->getMock();

        $testSubject->method('get')->willReturn($mockRegister);


        $testSubject->expects($this->exactly(3))->method('register')->with($mockRegister);

        $reflectionMethod = new ReflectionMethod(Cli::class, "initialize");
        $reflectionMethod->setAccessible(true);

        $reflectionMethod->invoke($testSubject);
    }
}
