<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Di\Injectable;
use UnitTester;

class InjectableTest extends Unit
{
    /** @var Injectable */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Injectable::class)->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
