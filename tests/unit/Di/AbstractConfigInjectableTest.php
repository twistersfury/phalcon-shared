<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Config\Theme;
use UnitTester;

class AbstractConfigInjectableTest extends Unit
{
    /** @var AbstractConfigInjectableTest */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractConfigInjectableTest::class)
            ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
