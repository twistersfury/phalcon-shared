<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di;

use Codeception\Test\Unit;
use Phalcon\Di\ServiceProviderInterface;
use ReflectionMethod;
use TwistersFury\Phalcon\Shared\Di\Http;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Config;
use UnitTester;

class HttpTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    public function testInitialize()
    {
        $mockRegister = $this->getMockBuilder(ServiceProviderInterface::class)
            ->onlyMethods(['register'])
            ->getMockForAbstractClass();

        $mockRegister->method('register')->willReturnSelf();

        $testSubject = $this->getMockBuilder(Http::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['get', 'register'])
            ->getMock();

        $testSubject->method('get')->willReturn($mockRegister);
        $testSubject->expects($this->exactly(8))->method('register')->with($mockRegister);

        $reflectionMethod = new ReflectionMethod(Http::class, "initialize");
        $reflectionMethod->setAccessible(true);

        $reflectionMethod->invoke($testSubject);
    }
}
