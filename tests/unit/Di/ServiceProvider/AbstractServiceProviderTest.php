<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Di\Di;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\AbstractServiceProvider;
use TwistersFury\Phalcon\Shared\Tests\Support\Mock\Shared\Di\ServiceProvider\TestProvider;
use UnitTester;

class AbstractServiceProviderTest extends Unit
{
    /** @var AbstractServiceProvider */
    private $testSubject = null;

    /** @var UnitTester */
    protected $tester = null;

    public function _before()
    {
        parent::_before();

        $this->testSubject = new TestProvider();
    }

    public function testRegister()
    {
        $di = new Di();
        $di->register($this->testSubject);

        $this->assertEquals('else', $di->get('something'));
    }
}
