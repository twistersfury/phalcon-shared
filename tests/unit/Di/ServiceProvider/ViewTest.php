<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Di\Di;
use Phalcon\Mvc\View as PhalconView;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\View;
use TwistersFury\Phalcon\Shared\Kernel;
use UnitTester;

class ViewTest extends Unit
{
    /** @var View */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before(): void
    {
        $this->testSubject = new View();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }

    public function testViewEmail()
    {
        $mockView = $this->makeEmpty(PhalconView::class);
        $mockKernel = $this->makeEmpty(
            Kernel::class,
            [
                'getThemeName' => 'theme'
            ]
        );

        $di = new Di();
        $di->register($this->testSubject);
        $di->setShared('kernel', $mockKernel);
        $di->setShared('view', $mockView);

        $this->tester->assertInstanceOf(
            PhalconView::class,
            $di->get(
                'viewEmail',
                [
                    'email',
                    'something'
                ]
            )
        );
    }
}
