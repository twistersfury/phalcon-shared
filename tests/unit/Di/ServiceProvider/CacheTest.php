<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Cache\Adapter\Memory;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Cache;
use UnitTester;

class CacheTest extends Unit
{
    /** @var Cache */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Cache();
    }

    public function testCache()
    {
        $this->tester->amGoingTo('Test Group Config Load');

        $config = new Config(
            [
                "cache" => [
                    "adapters" => [
                        "memory"
                    ]
                ]
            ]
        );

        $di = new Di();

        $di->set("config", $config);

        $di->register($this->testSubject);

        $this->assertTrue($di->has("cache"));

        $this->assertInstanceOf(Memory::class, $di->get("cache")->getAdapters()[0]);
    }
}
