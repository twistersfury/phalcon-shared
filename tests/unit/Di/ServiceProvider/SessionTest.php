<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\ServiceProvider;

use Codeception\Lib\Connector\Phalcon5\MemorySession;
use Codeception\Stub;
use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use Exception;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Logger\Logger;
use Phalcon\Session\Adapter\Noop;
use Phalcon\Session\Bag;
use Phalcon\Session\Manager;
use Phalcon\Tag;
use TwistersFury\Phalcon\Shared\Kernel;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Config as ConfigProvider;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Session;
use UnitTester;

class SessionTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    /** @var Session */
    private $testSubject = null;

    public function _before()
    {
        $di = new Di();
        $di->setShared(
            'config',
            new Config([
                'session' => [
                    'autoStart' => false,
                    'defaultAdapter' => MemorySession::class,
                    MemorySession::class => []
                ]
            ])
        );

        Di::setDefault($di);

        $this->testSubject = new Session();

        $this->testSubject->register($di);
    }

    public function _after()
    {
        if ($this->testSubject->get("session")) {
            $this->testSubject->get("session")->destroy();
        }
    }

    public function testSession()
    {
        $this->tester->amGoingTo("Test Default Session Adapter Loads");

        Di::getDefault()->setShared(
            'logger',
            $this->makeEmpty(
                Logger::class,
                [
                    'debug' => Expected::once (
                        'Initializing Sehssion',
                        [
                            'adapter' => MemorySession::class,
                            'config' => []
                        ]
                    )
                ]
            )
        );

        Di::getDefault()->set(Manager::class, $this->makeEmpty(Manager::class, ['start' => true]));

        $this->assertInstanceOf(Manager::class, $this->testSubject->get("session"));
    }

    public function testSessionException()
    {
        $this->tester->amGoingTo("Test Session Throws Exception For Missing Session");

        $this->tester->expectThrowable(
            new Exception("Session Adapter Doesn't Exist: Blah"),
            function () {
                $this->testSubject->set(
                    'config',
                    new Config([
                        'session' => [
                            'defaultAdapter' => 'Blah'
                        ]
                    ])
                );

                $this->testSubject->get("session");
            }
        );
    }


    public function testSessionExceptionInvalid()
    {
        $this->tester->amGoingTo("Test Session Throws Exception For Invalid Adapter");

        $this->tester->expectThrowable(
            new Exception("Invalid Adapter"),
            function () {
                $this->testSubject->set(
                    'config',
                    new Config([
                        'session' => [
                            'defaultAdapter' => Config::class,
                            Config::class => []
                        ]
                    ])
                );

                $this->testSubject->get("session");
            }
        );
    }

    public function testSessionDefault()
    {
        $mockKernel = $this->getMockBuilder(Kernel::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getConfigPath'])
            ->getMock();

        $mockKernel->method('getConfigPath')->willReturn(codecept_data_dir('/config'));

        $di = new Di();
        $di->set("kernel", $mockKernel);
        $di->register(new ConfigProvider());
        $di->set("tag", Tag::class);
        $di->setShared('logger', Stub::makeEmpty(Logger::class));
        $di->set(Manager::class, $this->makeEmpty(Manager::class, ['start' => true]));

        $this->testSubject->register($di);

        $this->assertInstanceOf(Manager::class, $this->testSubject->get("session"));
    }

    public function testSessionBag()
    {
        $this->tester->amGoingTo('Test Session Bag Is Registered');

        $mockSession = Stub::makeEmpty(Manager::class);

        $this->testSubject->setShared("session", $mockSession);

        $this->assertInstanceOf(Bag::class, $this->testSubject->get('sessionBag', ["blah"]));
    }
}
