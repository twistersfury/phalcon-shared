<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Db\Adapter\PdoFactory;
use Phalcon\Di\Di;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Database;
use UnitTester;

class DatabaseTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    /** @var Config */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = new Database();
    }

    public function testNoDatabases()
    {
        $this->tester->amGoingTo('Test Group Config Load');

        $mockConfig = $this->makeEmpty(Config::class);

        $di = new Di();
        $di->set('config', $mockConfig);
        $di->register($this->testSubject);

        $this->assertFalse($di->has("db"));
    }

    public function testNoConfig()
    {
        $this->tester->amGoingTo('Test Group Config Load');

        $di = new Di();
        $di->register($this->testSubject);

        $this->assertFalse($di->has("db"));
    }

    public function testSingleDatabase()
    {
        $this->tester->amGoingTo('Test Group Config Load');

        $mockConfig = new Config(
            [
                'databases' => [
                    'db' => [
                        'host'     => 'db.twistersfury.test',
                        'dbname'   => 'shared',
                        'port'     => 3306,
                        'username' => 'root',
                        'password' => 'root',
                        'adapter'  => 'mysql',
                    ]
                ]
            ]
        );

        $di = new Di();
        $di->set('config', $mockConfig);
        $di->set(PdoFactory::class, $this->makeEmpty(PdoFactory::class));
        $di->register($this->testSubject);

        $this->assertTrue($di->has("db"));
    }
}
