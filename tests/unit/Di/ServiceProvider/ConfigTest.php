<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Di\Di;
use TwistersFury\Phalcon\Shared\Kernel;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Config;
use UnitTester;

class ConfigTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    /** @var Config */
    private $testSubject = null;

    public function _before()
    {
        $this->testSubject = new Config();
    }

    public function testConfig()
    {
        $this->tester->amGoingTo('Test Group Config Load');

        $mockKernel = $this->getMockBuilder(Kernel::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getConfigPath'])
            ->getMockForAbstractClass();

        $mockKernel->expects($this->once())->method('getConfigPath')
            ->willReturn(codecept_root_dir('tests/_data/config'));

        $di = new Di();
        $di->set('kernel', $mockKernel);
        $di->register($this->testSubject);

        $config = $di->get('config');

        $this->assertEquals(1, $config->A);
        $this->assertEquals(2, $config->B);
    }
}
