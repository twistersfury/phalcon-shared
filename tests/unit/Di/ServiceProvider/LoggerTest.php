<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Logger\Logger as phLogger;
use Phalcon\Logger\Adapter\Noop;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Logger;
use UnitTester;

class LoggerTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    /** @var Logger */
    private $testSubject = null;

    public function _before()
    {
        $di = new Di();
        $di->set(
            'config',
            new Config([
                'logger' => [
                    'defaultAdapter' => Noop::class,
                    'adapters' => [
                        Noop::class
                    ],
                    Noop::class => []
                ]
            ])
        );

        Di::setDefault($di);

        $this->testSubject = new Logger();

        $this->testSubject->register($di);
    }

    public function testLogger()
    {
        $this->tester->amGoingTo("Test Default Logger Adapter Loads");

        $this->assertInstanceOf(\Phalcon\Logger\Logger::class, $this->testSubject->get("logger"));
    }

    public function testAdapters()
    {
        $this->tester->amGoingTo("Test Additional Adapters Are Added");

        $mockLogger = $this->getMockBuilder(phLogger::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['addAdapter'])
            ->getMock();

        $mockLogger->expects($this->exactly(2))->method('addAdapter')->with(
            Noop::class,
            $this->isInstanceOf(Noop::class)
        );

        Di::getDefault()->set(
            "Phalcon\Logger\Logger",
            $mockLogger
        );

        $this->assertInstanceOf(\Phalcon\Logger\Logger::class, $this->testSubject->get("logger"));
    }
}
