<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Theme;
use UnitTester;

class ThemeTest extends Unit
{
    /** @var Theme */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Theme::class)
            ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
