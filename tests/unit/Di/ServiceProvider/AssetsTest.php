<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Di\ServiceProvider;

use Codeception\Test\Unit;
use Phalcon\Assets\Manager;
use Phalcon\Cache\Adapter\Memory;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Html\TagFactory;
use TwistersFury\Phalcon\Shared\Config\Theme;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\Assets;
use TwistersFury\Phalcon\Shared\Kernel;
use UnitTester;

class AssetsTest extends Unit
{
    /** @var Assets */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new Assets();
    }

    public function testAssets()
    {
        $mockKernel = $this->makeEmpty(
            Kernel::class,
            [
                "getThemeConfig" => $this->makeEmpty(Theme::class)
            ]
        );

        $di = new Di();

        $di->set("kernel", $mockKernel);
        $di->set("tag", $this->makeEmpty(TagFactory::class));

        $di->register($this->testSubject);

        $this->assertTrue($di->has("assets"));

        $this->assertInstanceOf(Manager::class, $di->get("assets"));
    }
}
