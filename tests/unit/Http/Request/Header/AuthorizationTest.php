<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Http\Request\Header;

use Codeception\Test\Unit;
use Phalcon\Di\Di;
use Phalcon\Http\Request;
use TwistersFury\Phalcon\Shared\Http\Request\Header\Authorization;
use UnitTester;

class AuthorizationTest extends Unit
{
    /** @var Authorization */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $di = new Di();

        $di->set(
            'request',
            $this->makeEmpty(
                Request::class,[
                    'getHeaders' => ['Authorization' => 'Bearer ABC123']
                ]
            )
        );

        $this->testSubject = new Authorization();
        $this->testSubject->setDi($di);
        $this->testSubject->initialize();
    }

    public function testBasics()
    {
        $this->assertTrue($this->testSubject->isBearer());
        $this->assertFalse($this->testSubject->isDigest());
        $this->assertFalse($this->testSubject->isBasic());

        $this->assertEquals('BEARER', $this->testSubject->getType());
        $this->assertEquals('ABC123', $this->testSubject->getToken());
    }
}
