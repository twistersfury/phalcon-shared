<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Cli;

use Phalcon\Cli\Console;
use TwistersFury\Phalcon\Shared\Cli\Kernel;
use TwistersFury\Phalcon\Shared\Tests\Unit\KernelTest as BaseKernelTest;

class KernelTest extends BaseKernelTest
{
    protected function getKernelClass(): string
    {
        return Kernel::class;
    }

    protected function getApplicationClass(): string
    {
        return Console::class;
    }
}
