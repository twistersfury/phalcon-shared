<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\AbstractDummy;

use TwistersFury\Phalcon\Shared\AbstractDummy\AbstractDummy;
use Codeception\Test\Unit;

/**
 * Dummy Class To Help Resolve Dependencies Due To Error
 *
 * @see https://github.com/zephir-lang/zephir/issues/2142
 */
class AbstractDummyTest extends Unit
{
    public function testDummy()
    {
        $this->assertTrue(class_exists(AbstractDummy::class));
    }
}
