<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Cache;

use Codeception\Stub;
use Codeception\Test\Unit;
use Phalcon\Cache\Adapter\Memory;
use Phalcon\Cache\Adapter\Stream;
use Phalcon\Storage\Serializer\None;
use Phalcon\Storage\SerializerFactory;
use TwistersFury\Phalcon\Shared\Cache\Grouped;
use UnitTester;

class GroupedTest extends Unit
{
    /** @var Grouped */
    private $testSubject;

    /** @var Stream */
    private $mockAdapter;

    /** @var Stream */
    private $mockSecondAdapter;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->mockAdapter = $this->makeEmpty(
            Stream::class,
            [
                'has' => false,
                'get' => function () {
                    return null;
                }
            ]
        );

        $this->mockSecondAdapter = $this->makeEmpty(
            Stream::class,
            [
                'has' => true,
                'get' => function () {
                    return 'Some Value';
                }
            ]
        );

        $this->testSubject = new Grouped(
            [
                $this->mockAdapter,
                $this->mockSecondAdapter
            ]
        );
    }

    public function testGetAdapters() {
        $this->assertSame($this->mockAdapter, $this->testSubject->getAdapters()[0]);
        $this->assertSame($this->mockSecondAdapter, $this->testSubject->getAdapters()[1]);
    }

    public function testGet()
    {
        $this->assertEquals('Some Value', $this->testSubject->get('key'));
    }

    public function testSet()
    {
        $firstAdapter = new Memory(new SerializerFactory());
        $secondAdapter = new Memory(new SerializerFactory());

        $firstAdapter->set('key', 'Nope');
        $secondAdapter->set('key', 'Yep');

        $this->testSubject = new Grouped(
            [
                $firstAdapter,
                $secondAdapter
            ]
        );

        $this->assertTrue($this->testSubject->set('key', 'Some Value'));

        $this->assertEquals('Some Value', $this->testSubject->get('key', "Not Set"));
        $this->assertEquals('Some Value', $firstAdapter->get('key'));
        $this->assertEquals('Some Value', $secondAdapter->get('key'));
    }
}
