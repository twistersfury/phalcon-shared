<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Mvc\Model\Behavior;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Mvc\Model\Behavior\Base64;
use UnitTester;

class Base64Test extends Unit
{
    /** @var Base64 */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Base64::class)
                                  ->disableOriginalConstructor()
                                  ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }



}
