<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Mvc\Model\Behavior;

use Codeception\Test\Unit;
use Phalcon\Di\DiInterface;
use Phalcon\Mvc\Model;
use TwistersFury\Phalcon\Shared\Mvc\Model\Behavior\DateTime;
use UnitTester;

class DateTimeTest extends Unit
{
    /** @var DateTime */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = new DateTime([
            'fields' => [
                'updatedAt'
            ]
        ]);
    }

    public function testAfterWithDefault()
    {
        $mockDi = $this->createMock(DiInterface::class);
        $mockDi->expects($this->once())
            ->method('get')
            ->with('\DateTime', [null])
            ->willReturn(new \DateTime());

        $mockModel = $this->createMock(Model::class);
        $mockModel->expects($this->once())
            ->method('writeAttribute')
            ->with(
                'updatedAt',
                $this->isInstanceOf(\DateTime::class)
            );

        $mockModel->expects($this->once())
            ->method('readAttribute')
            ->with('updatedAt')
            ->willReturn('current_timestamp()');

        $mockModel->expects($this->once())
            ->method('getDi')->willReturn(
                $mockDi
            );

        $this->testSubject->notify('afterFetch', $mockModel);
    }

    public function testAfter()
    {
        $mockDi = $this->createMock(DiInterface::class);
        $mockDi->expects($this->once())
            ->method('get')
            ->with('\DateTime', ['2024-08-04'])
            ->willReturn(new \DateTime());

        $mockModel = $this->createMock(Model::class);
        $mockModel->expects($this->once())
            ->method('writeAttribute')
            ->with(
                'updatedAt',
                $this->isInstanceOf(\DateTime::class)
            );

        $mockModel->expects($this->once())
            ->method('readAttribute')
            ->with('updatedAt')
            ->willReturn('2024-08-04');

        $mockModel->expects($this->once())
            ->method('getDi')->willReturn(
                $mockDi
            );

        $this->testSubject->notify('afterFetch', $mockModel);
    }

    public function testSave()
    {
        $mockModel = $this->createMock(Model::class);
        $mockModel->expects($this->once())
            ->method('writeAttribute')
            ->with(
                'updatedAt',
                '2024-08-04 00:00:00'
            );

        $mockModel->expects($this->once())
            ->method('readAttribute')
            ->with('updatedAt')
            ->willReturn(new \DateTime('2024-08-04'));

        $this->testSubject->notify('prepareSave', $mockModel);
    }

    public function testConvertedAlready()
    {
        $mockModel = $this->createMock(Model::class);
        $mockModel->expects($this->never())
            ->method('writeAttribute');

        $mockModel->expects($this->once())
            ->method('readAttribute')
            ->with('updatedAt')
            ->willReturn(new \DateTime('2024-08-04'));

        $this->testSubject->notify('afterFetch', $mockModel);
    }
}
