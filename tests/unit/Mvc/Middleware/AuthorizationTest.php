<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Mvc\Middleware;

use Codeception\Test\Unit;
use Phalcon\Config\Config;
use Phalcon\Di\Di;
use Phalcon\Events\Event;
use Phalcon\Http\Request;
use Phalcon\Mvc\Dispatcher;
use TwistersFury\Phalcon\Shared\Http\Request\Header\Authorization as AuthorizationHeader;
use TwistersFury\Phalcon\Shared\Mvc\Middleware\Authorization;
use UnitTester;

class AuthorizationTest extends Unit
{
    /** @var Authorization */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $di = new Di();

        $di->set(
            'TwistersFury\Phalcon\Shared\Http\Request\Header\Authorization',
            $this->makeEmpty(
                AuthorizationHeader::class,[
                    'getToken' => 'abc123'
                ]
            )
        );

        $di->set(
            'config',
            new Config([
                'auth' => [
                    'token' => 'abc123'
                ]
            ])
        );

        $this->testSubject = new Authorization();
        $this->testSubject->setDi($di);
        $this->testSubject->initialize();
    }

    public function testBasics()
    {
        $mockEvent = $this->makeEmpty(Event::class);
        $mockDispatcher = $this->makeEmpty(Dispatcher::class);

        $this->assertTrue($this->testSubject->beforeExecuteRoute($mockEvent, $mockDispatcher));
    }
}
