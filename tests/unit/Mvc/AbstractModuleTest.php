<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Mvc;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Mvc\AbstractModule;
use UnitTester;

class AbstractModuleTest extends Unit
{
    /** @var AbstractModule */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractModule::class)
                                  ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
