<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Mvc\Controller;

use Codeception\Lib\Connector\Phalcon5\MemorySession;
use Codeception\Lib\Connector\Phalcon5\SessionManager;
use Codeception\Test\Unit;
use Phalcon\Di\Di;
use Phalcon\Session\Bag;
use TwistersFury\Phalcon\Shared\Mvc\Controller\AbstractController;
use UnitTester;

class AbstractControllerTest extends Unit
{
    /** @var UnitTester */
    protected $tester;

    /** @var AbstractController */
    private $testSubject;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractController::class)
            ->disableOriginalConstructor()
            ->getMockForAbstractClass();

        $di = new Di();
        Di::setDefault($di);

        $this->testSubject->setDi($di);
    }

    public function testPersistent()
    {
        $session = $this->tester->buildSession();

        $this->testSubject->getDi()->set("session", $session);
        $this->testSubject->getDI()->set("sessionBag", new Bag($session, "test"));

        $sessionBag = $this->testSubject->persistent;

        $this->assertTrue($sessionBag->has("_isInitialized"));
    }
}
