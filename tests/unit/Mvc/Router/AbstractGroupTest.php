<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Mvc\Router;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Mvc\Router\AbstractGroup;
use UnitTester;

class AbstractGroupTest extends Unit
{
    /** @var AbstractGroup */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(AbstractGroup::class)
                                  ->disableOriginalConstructor()
                                  ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
