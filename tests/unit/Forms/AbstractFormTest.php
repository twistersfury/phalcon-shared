<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Forms;

use Codeception\Test\Unit;
use Generator;
use Phalcon\Di\Di;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Forms\Element\Text;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Url;
use TwistersFury\Phalcon\Shared\Forms\AbstractForm;
use TwistersFury\Phalcon\Shared\Forms\Element\Captcha;
use TwistersFury\Phalcon\Shared\Tests\Support\Mock\Shared\Forms\TestForm;
use UnitTester;

class AbstractFormTest extends Unit
{
    private AbstractForm $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        Di::setDefault(new Di());

        Di::getDefault()->set(
            'dispatcher',
            $this->makeEmpty(
                Dispatcher::class,
                [
                    'getModuleName' => 'module',
                    'getControllerName' => 'controller',
                ]
            )
        );

        Di::getDefault()->set(
            Captcha::class,
            $this->makeEmpty(Captcha::class)
        );

        Di::getDefault()->set(
            'url',
            $this->makeEmpty(
                Url::class,
            )
        );

        $this->testSubject = new TestForm();

        $this->testSubject->initialize();
    }

    /**
     * @param string $methodName
     * @param string $className
     * @return void
     *
     * @dataProvider _dpElement
     */
    public function testElement(string $methodName, string $className): void
    {
        $reflectionMethod = new \ReflectionMethod($this->testSubject, $methodName);
        $reflectionMethod->setAccessible(true);

        $this->assertInstanceOf(
            $className,
            $reflectionMethod->invoke($this->testSubject, 'element')
        );
    }

    public function testSelect(): void
    {
        $reflectionMethod = new \ReflectionMethod($this->testSubject, 'buildSelect');
        $reflectionMethod->setAccessible(true);

        $this->assertInstanceOf(
            Select::class,
            $reflectionMethod->invoke(
                $this->testSubject,
                'element',
                $this->makeEmpty(Resultset::class)
            )
        );
    }

    public function _dpElement(): Generator
    {
        yield 'Text' => [
            'buildText',
            Text::class
        ];

        yield 'Textarea' => [
            'buildTextarea',
            TextArea::class
        ];

        yield 'Checkbox' => [
            'buildCheckbox',
            Check::class
        ];

        yield 'Captcha' => [
            'buildCaptcha',
            Captcha::class
        ];

        yield 'Submit' => [
            'buildSubmit',
            Submit::class
        ];
    }
}
