<?php

namespace TwistersFury\Phalcon\Shared\Tests\Unit\Forms\Element;

use Codeception\Test\Unit;
use TwistersFury\Phalcon\Shared\Forms\Element\Captcha;
use UnitTester;

class CaptchaTest extends Unit
{
    /** @var Captcha */
    private $testSubject;

    /** @var UnitTester */
    protected $tester;

    public function _before()
    {
        $this->testSubject = $this->getMockBuilder(Captcha::class)
                                  ->disableOriginalConstructor()
                                  ->getMockForAbstractClass();
    }

    public function testDummy()
    {
        $this->tester->markTestIncomplete('Need To Implement Testing');
    }
}
