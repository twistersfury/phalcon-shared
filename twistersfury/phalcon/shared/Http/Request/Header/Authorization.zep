namespace TwistersFury\Phalcon\Shared\Http\Request\Header;

use Phalcon\Di\Injectable;
use Phalcon\Di\InitializationAwareInterface;

class Authorization extends Injectable implements InitializationAwareInterface
{
    private authType = "";
    private authToken = "";

    public function initialize() -> void
    {
        var header = explode(" ", this->{"request"}->getHeaders()["Authorization"]);

        let this->authType = strtoupper(header[0]);
        let this->authToken = header[1];
    }

    public function getToken() -> string
    {
        return this->authToken;
    }

    public function getType() -> string
    {
        return this->authType;
    }

    public function isBearer() -> bool
    {
        return this->getType() === "BEARER";
    }

    public function isBasic() -> bool
    {
        return this->getType() === "BASIC";
    }

    public function isDigest() -> bool
    {
        return this->getType() === "DIGEST";
    }
}
