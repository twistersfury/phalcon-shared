namespace TwistersFury\Phalcon\Shared\Mvc\Middleware;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Di\Injectable;
use Phalcon\Di\InitializationAwareInterface;
use Phalcon\Di\InitializationAwareInterface;
use TwistersFury\Phalcon\Shared\Http\Request\Header\Authorization as AuthorizationHeader;


class Authorization extends Injectable implements InitializationAwareInterface
{
    private authToken;

    public function initialize() -> void
    {
        let this->authToken = this->buildAuth();
    }

    public function beforeExecuteRoute(<Event> event, <Dispatcher> dispatcher) -> bool
    {
        return this->isValid(this->getToken());
    }

    protected function getToken()
    {
        return this->authToken->getToken();
    }

    private function buildAuth() -> <AuthorizationHeader>
    {
        return this->getDi()->get("TwistersFury\Phalcon\Shared\Http\Request\Header\Authorization");
    }

    protected function isValid(string! bearerToken) -> bool
    {
        return bearerToken === this->getDi()->get("config")->auth->token;
    }
}
