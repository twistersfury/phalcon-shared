namespace TwistersFury\Phalcon\Shared\Mvc\Router;

use Phalcon\Mvc\Router\RouteInterface;
use Phalcon\Mvc\Router\Group;

abstract class AbstractGroup extends Group
{
    const SEGMENT_MODULE = 2;

    /**
     * @throws \ReflectionException
     */
    public function initialize()
    {
        this->setPrefix(this->buildPrefix())
             ->setPaths(
                 [
                     "module"     : this->getModule(),
                     "controller" : this->getController()
                 ]
             );
    }

    protected function hasParent() -> bool
    {
        return false;
    }

    protected function hasEntity() -> bool
    {
        return false;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function buildPrefix() -> string
    {
        string routePrefix;

        let routePrefix = "/" . this->getModule() . "/";

        if this->hasParent() {
            if this->getParentController() {
                let routePrefix .= this->getParentController() . "/";
            }

            let routePrefix .= "{parentEntity:\\d+}/";
        }

        let routePrefix .= this->getController();

        return routePrefix;
    }


    protected function explodeClass() -> array
    {
        return explode("\\", get_called_class());
    }

    protected function getModule() -> string
    {
        return this->prepareSegment(
            this->explodeClass()[this->getModuleSegment()]
        );
    }

    protected function getModuleSegment() -> int
    {
        return AbstractGroup::SEGMENT_MODULE;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    protected function getController() -> string
    {
        return this->prepareSegment(
            str_replace(
                "Group",
                "",
                (new \ReflectionClass(get_called_class()))->getShortName()
            )
        );
    }

    protected function getParentController() -> string | null
    {
        return null;
    }

    protected function prepareSegment(string routeSegment) -> string
    {
        return str_replace("_", "-", uncamelize(routeSegment));
    }

    public function convertEntity(entityId)
    {
        return entityId;
    }

    public function convertParentEntity(parentEntity)
    {
        return parentEntity;
    }

    protected function addRoute(pattern, paths = null, httpMethods = null) -> <RouteInterface>
    {
        /** @var \Phalcon\Mvc\Router\Route $route */
        var route = parent::addRoute(pattern, paths, httpMethods);

        if ($this->hasEntity()) {
            route->convert(
                "entity",
                [
                    this,
                    "convertEntity"
                ]
            );
        }

        if (this->hasParent()) {
            route->convert(
                "parentEntity",
                [
                    this,
                    "convertParentEntity"
                ]
            );
        }

        return route;
    }
}
