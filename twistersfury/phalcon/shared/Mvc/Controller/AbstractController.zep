namespace TwistersFury\Phalcon\Shared\Mvc\Controller;

use Phalcon\Http\Message\UploadedFile;
use Phalcon\Mvc\Controller;
use Phalcon\Session\Bag;

abstract class AbstractController extends Controller
{
    protected _persistent;

    public function initialize()
    {
        this->loadAsset("inlineScripts", "addInlineJs")
            ->loadAsset("scripts", "addJs")
            ->loadAsset("stylesheets", "addCss")
            ->configureTitle();
    }

    private function loadAsset(string! configName, string! methodName) -> <AbstractController>
    {
        if !this->getDi()->has("themeConfig") || !this->{"themeConfig"}->has(configName) {
            return this;
        }

        var filePath;

        for filePath in this->{"themeConfig"}->get(configName)->toArray() {
            this->{"assets"}->{methodName}(filePath);
        }

        return this;
    }

    protected function getTitle() -> string
    {
        return this->{"router"}->getControllerName();
    }

    protected function getModule() -> string
    {
        return this->{"router"}->getModuleName();
    }

    protected function configureTitle() -> <AbstractController>
    {
        this->{"tag"}->title()->set(this->getTitle());
        this->{"tag"}->title()->append(this->getModule());
        this->{"tag"}->title()->append(this->{"config"}->system->name);
        this->{"tag"}->title()->setSeparator(this->{"config"}->system->separator);

        return this;
    }

    public function __get(string! propertyName) -> var | null
    {
        if propertyName === "persistent" {
            return this->getPersistent();
        }

        return parent::__get(propertyName);
    }

    protected function getPersistent() -> <Bag>
    {
        if this->_persistent === null {
            let this->_persistent = this->getDi()->get("sessionBag", [get_class(this)]);
        }

        if !this->_persistent->has("_isInitialized") {
            this->initializePersistent(this->_persistent);
        }

        return this->_persistent;
    }

    protected function initializePersistent(<Bag> sessionBag) -> void
    {
        let sessionBag->_isInitialized = true;
    }

    protected function getUploadedFile(string! imageName) -> <UploadedFile>
    {
        var item;

        this->{"logger"}->debug("Get Uploaded File", [
            "image": imageName,
            "images": this->getUploadedFiles()
        ]);

        if !fetch item, this->getUploadedFiles()[imageName] {
            throw new \Exception("Not Found");
        }

        return item;
    }

    protected function getUploadedFiles() -> array
    {
        return this->{"serverRequest"}->getUploadedFiles();
    }
}
