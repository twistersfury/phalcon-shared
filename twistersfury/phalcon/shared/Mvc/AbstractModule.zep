namespace TwistersFury\Phalcon\Shared\Mvc;

use Phalcon\Di\DiInterface;
use Phalcon\Mvc\ModuleDefinitionInterface;

abstract class AbstractModule implements ModuleDefinitionInterface
{
    abstract protected function getModuleName() -> string;
    abstract protected function getControllerNamespace() -> string;

    public function registerAutoloaders(<DiInterface> dependencyInjector = null) -> void
    {
        //Nothing To Do Here
    }

    public function registerServices(<DiInterface> dependencyInjector) -> void
    {
        dependencyInjector->get("dispatcher")->setModuleName(this->getModuleName());
        dependencyInjector->get("dispatcher")->setDefaultNamespace(this->getControllerNamespace());
    }
}
