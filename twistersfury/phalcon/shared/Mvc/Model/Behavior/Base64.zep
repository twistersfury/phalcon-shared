namespace TwistersFury\Phalcon\Shared\Mvc\Model\Behavior;

use InvalidArgumentException;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\ModelInterface;

class Base64 extends Behavior
{
    public function notify(string! type, <ModelInterface> model)
    {
        switch (type) {
            case "afterSave":
            case "afterFetch":
                this->decode(model);

                break;
            case "prepareSave":
                this->encode(model);

                break;
        }
    }

    private function decode(<ModelInterface> model) -> void
    {
        var field;

        for field in this->getFields() {
            model->{"writeAttribute"}(
                field,
                base64_decode(model->{"readAttribute"}(field))
            );
        }
    }

    private function encode(<ModelInterface> model) -> void
    {
        var field;

        for field in this->getFields() {
            model->{"writeAttribute"}(
                field,
                base64_encode(model->{"readAttribute"}(field))
            );
        }
    }

    private function getFields() -> array
    {
        var fields;

        if unlikely !fetch fields, this->getOptions()["fields"] || !is_array(fields) {
            throw new InvalidArgumentException("Missing/Invalid 'fields' Configuration");
        }

        return fields;
    }
}
