namespace TwistersFury\Phalcon\Shared\Mvc\Model\Behavior;

use \DateTime as phpDateTime;
use InvalidArgumentException;
use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\ModelInterface;

class DateTime extends Behavior
{
    public function notify(string! type, <ModelInterface> model)
    {
        switch (type) {
            case "afterSave":
            case "afterFetch":
                this->convertToDate(model);

                break;
            case "prepareSave":
                this->convertToString(model);

                break;
        }
    }

    private function convertToDate(<ModelInterface> model) -> void
    {
        var field, currentValue;

        for field in this->getFields() {
            let currentValue = model->{"readAttribute"}(field);

            if is_object(currentValue) && currentValue instanceof phpDateTime {
                continue;
            }

            if stripos(currentValue, "current_timestamp") !== false {
                let currentValue = null;
            }

            model->{"writeAttribute"}(
                field,
                model->{"getDi"}()->get(
                    "\DateTime",
                    [
                        currentValue
                    ]
                )
            );
        }
    }

    private function convertToString(<ModelInterface> model) -> void
    {
        var field, value;

        for field in this->getFields() {
            let value = model->{"readAttribute"}(field);

            if is_object(value) && value instanceof phpDateTime {
                model->{"writeAttribute"}(
                    field,
                    value->format("Y-m-d H:i:s")
                );
            }
        }
    }

    private function getFields() -> array
    {
        var fields;

        if unlikely !fetch fields, this->getOptions()["fields"] || !is_array(fields) {
            throw new InvalidArgumentException("Missing/Invalid 'fields' Configuration");
        }

        return fields;
    }
}
