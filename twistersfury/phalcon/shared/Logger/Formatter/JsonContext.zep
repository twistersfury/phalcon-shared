namespace TwistersFury\Phalcon\Shared\Logger\Formatter;

use Phalcon\Di\Di;
use Phalcon\Logger\Item;
use Phalcon\Logger\Formatter\AbstractFormatter;

/**
 * TwistersFury\Phalcon\Shared\Logger\Formatter\JsonContext
 *
 * Formats messages using JSON encoding, but adds index for context
 */
class JsonContext extends AbstractFormatter
{
    /**
     * Phalcon\Logger\Formatter\Json construct
     */
    public function __construct(string dateFormat = "c")
    {
        let this->dateFormat = dateFormat;
    }

    /**
     * Applies a format to a message before sent it to the internal log
     */
    public function format(<Item> item) -> string
    {
        var message;

        let message = item->getMessage();

        return Di::getDefault()->get("helper")->encode(
            [
                "type"      : item->getLevel(),
                "message"   : message,
                "timestamp" : item->getDateTime()->format("Y-m-d H:i:s"),
                "context"   : item->getContext()
            ]
        );
    }
}
