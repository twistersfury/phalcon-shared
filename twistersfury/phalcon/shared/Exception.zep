namespace TwistersFury\Phalcon\Shared;

use Throwable;
use RuntimeException;

class Exception extends RuntimeException implements Throwable
{

}
