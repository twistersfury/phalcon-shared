namespace TwistersFury\Phalcon\Shared\Di;

use Phalcon\Di\FactoryDefault\Cli as FactoryDefault;
use TwistersFury\ChatBot\Connection\Manager;
use Exception;

class Cli extends FactoryDefault
{
    public function initialize() -> void
    {
        this->register(this->get("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Logger"));
        this->register(this->get("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Config"));
        this->register(this->get("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Cache"));
    }
}
