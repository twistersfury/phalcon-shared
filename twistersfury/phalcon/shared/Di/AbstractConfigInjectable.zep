namespace TwistersFury\Phalcon\Shared\Di;

use Phalcon\Config\ConfigInterface;
use Phalcon\Di\Di;
use Phalcon\Events\EventsAwareInterface;
use Serializable;
use TwistersFury\Phalcon\Shared\Di\Injectable;

abstract class AbstractConfigInjectable extends Injectable implements Serializable
{
    private config;

    public function __construct(<ConfigInterface> config = null)
    {
        this->setConfig(config);
    }

    public function __wakeup() -> void
    {
        this->setDi(Di::getDefault());

        if this instanceof EventsAwareInterface {
            this->{"setEventsManager"}(this->getDi()->get("eventsManager"));
        }
    }

    public function setConfig(<ConfigInterface> config) -> <AbstractConfigInjectable>
    {
        let this->config = config;

        return this;
    }

    public function getConfig() -> <ConfigInterface>
    {
        return this->config;
    }

    /**
     * Note: Per PHP Docs, this will not be called if the class defines __serialize
     */
    public function serialize() -> string
    {
        return serialize(this->getConfig());
    }

    public function unserialize(var data) -> void
    {
        this->setConfig(
            unserialize(data)
        );
    }
}
