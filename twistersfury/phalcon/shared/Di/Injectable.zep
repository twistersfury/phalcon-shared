namespace TwistersFury\Phalcon\Shared\Di;

use Phalcon\Config\ConfigInterface;
use Phalcon\Di\Injectable as phInjectable;
use Phalcon\Events\ManagerInterface;
use Phalcon\Logger\Logger;
use Psr\SimpleCache\CacheInterface;
use TwistersFury\Phalcon\Shared\Kernel;
use TwistersFury\Phalcon\Shared\Di\Interfaces\InitializationAware;

abstract class Injectable extends phInjectable implements InitializationAware
{
    /**
     * Get The DI Level [Application] Configuration
     */
    public function getApplicationConfig() -> <ConfigInterface>
    {
        return this->getDi()->get("config");
    }

    /**
     * @deprecated Replaced by `getApplicationConfig`
     */
    public function getConfig() -> <ConfigInterface>
    {
        trigger_error("Use `getApplicationConfig`", E_USER_DEPRECATED);

        return this->getConfig();
    }

    public function getCache(string! serviceName = "cache") -> <CacheInterface>
    {
        return this->getDi()->get(serviceName);
    }

    public function getEventsManager() -> <ManagerInterface> | null //Null to match Interface
    {
        return this->getDi()->get("eventsManager");
    }

    public function getKernel() -> <Kernel>
    {
        return this->getDi()->get("kernel");
    }

    public function getLogger() -> <Logger>
    {
        return this->getDi()->get("logger");
    }

    public function initialize() -> void
    {
        //Nothing To Do Here...Right Now...
    }
}
