namespace TwistersFury\Phalcon\Shared\Di;

use Phalcon\Di\FactoryDefault;
use TwistersFury\Phalcon\Shared\Di\Interfaces\InitializationAware;

class Http extends FactoryDefault implements InitializationAware
{
    public function initialize() -> void
    {
        this->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Logger")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Config")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Cache")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Session")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Url")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Assets")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Mvc")
            ->addServiceProvider("TwistersFury\\Phalcon\\Shared\\Di\\ServiceProvider\\Theme");
    }

    protected function addServiceProvider(string className) -> <Http>
    {
        this->register(this->get(className));

        return this;
    }

    public function get(string! name, parameters = null) -> var
    {
        var instance = parent::get(name, parameters);

        if instance instanceof InitializationAware {
            instance->{"initialize"}();
        }

        return instance;
    }
}
