namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Di\DiInterface;
use Phalcon\Logger\Logger;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;
use RuntimeException;

class Config extends AbstractServiceProvider
{
    public function getConfigPath() -> string
    {
        if unlikely !this->has("kernel") && this->has("configPath") {
            return this->get("configPath"); // Assuming In Testing
        }

        return this->getShared("kernel")->getConfigPath();
    }

    public function getLoggerPath() -> string
    {
        var loggerPath = "";
        if unlikely !this->has("kernel") && this->has("loggerPath") {
            return this->get("loggerPath"); // Assuming In Testing
        }

        return this->getShared("kernel")->getLoggerPath();
    }

    protected function registerConfig() -> void
    {
        var self = this;

        this->setShared(
            "config",
            function () use (self) {
                return self->buildConfig();
            }
        );
    }

    protected function registerDefaultConfig() -> void
    {
        this->attempt(
            "defaultConfig",
            function () {
                return this->{"get"}("Phalcon\\Config\\Config", []);
            }
        );
    }

    protected function buildConfig() -> <Config>
    {
        //Base Configuration
        array configFiles = [
            this->get("baseConfig"),
            this->get("defaultConfig")
        ];

        var configPath = this->getConfigPath();

        if unlikely !file_exists(configPath . "/dist") {
            throw new RuntimeException("Config (Dist) Does Not Exist");
        }

        var recursiveIterator, file;

        let recursiveIterator = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator(
                configPath . "/dist",
                RecursiveDirectoryIterator::SKIP_DOTS
            )
        );

        for file in recursiveIterator {
            if likely substr(file->getBasename(), 0, 7) === "config." {
                let configFiles[] = file->getPathname();
            }
        }

        if file_exists(configPath . "/local") {
            let recursiveIterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator(
                    configPath . "/local",
                    RecursiveDirectoryIterator::SKIP_DOTS
                )
            );

            for file in recursiveIterator {
                if likely substr(file->getBasename(), 0, 7) === "config." {
                    let configFiles[] = file->getPathname();
                }
            }
        }

        if likely this->has("eventsManager") {
            var config;
            let config = this->get("eventsManager")->fire("config:init", this, configFiles);
            if is_array(config) {
                let configFiles = array_merge(
                    configFiles,
                    config
                );
            }
        }

        return this->get("Phalcon\\Config\\Adapter\\Grouped", [configFiles]);
    }

    public function registerBaseConfig() -> void
    {
        this->{"set"}(
            "baseConfig",
            this->{"get"}(
                "Phalcon\\Config\\Config",
                [
                    [
                        "modules": [
                            "support": [
                                "className": "TwistersFury\\Phalcon\\Shared\\Support\\Mvc\\Module"
                            ]
                        ],
                        "system": [
                            "name": "[Change Config Name]",
                            "separator": " | ",
                            "theme": "default",
                            "baseUri" : null,
                            "staticUri" : null
                        ],
                        "routes": [
                            "defaults": [
                                "module": "support",
                                "controller":"system",
                                "action":"index"
                            ],
                            "defaultRoute": [
                                "module": "support",
                                "controller": "home",
                                "action":"index"
                            ],
                            "notFound": [
                                "module": "support",
                                "controller":"system",
                                "action":"noRoute"
                            ],
                            "groups":[
                                "TwistersFury\\Phalcon\\Shared\\Support\\Mvc\\Router\\Group\\Assets"
                            ]
                        ],
                        "session": [
                            "autoStart" : false,
                            "defaultAdapter": getenv("ENV_SESSION_ADAPTER") ? getenv("ENV_SESSION_ADAPTER") : "Phalcon\\Session\\Adapter\\Stream",
                            "Phalcon\\Session\\Adapter\\Stream": [
                                "savePath" : "/tmp"
                            ],
                            "Phalcon\\Session\\Adapter\\Noop": [
                            ]
                        ],
                        "forms": [],
                        "logger" : [
                            "name": "application",
                            "defaultAdapter": "Phalcon\\Logger\\Adapter\\Stream",
                            "formatter": "TwistersFury\\Phalcon\\Shared\\Logger\\Formatter\\JsonContext",
                            "logLevel": Logger::WARNING,
                            "Phalcon\\Logger\\Adapter\\Stream": [
                                this->getLoggerPath() . "/phalcon.log"
                            ],
                            "TwistersFury\\Phalcon\\Shared\\Logger\\Formatter\\JsonContext": []
                        ]
                    ]
                ]
            )
        );
    }
}
