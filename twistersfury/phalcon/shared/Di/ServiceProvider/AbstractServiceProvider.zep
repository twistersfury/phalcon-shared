namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Di\DiInterface;
use Phalcon\Di\AbstractInjectionAware;
use Phalcon\Di\Service;
use Phalcon\Di\ServiceProviderInterface;

/**
 * Class AbstractServiceProviderTest
 *
 * @package TwistersFury\Tests\Unit\Shared\Di\ServiceProvider
 * @method \Phalcon\Di\Service getService(string $name);
 * @abstract
 */
abstract class AbstractServiceProvider extends AbstractInjectionAware implements ServiceProviderInterface
{
    public function register(<DiInterface> di) -> void
    {
        this->setDi(di);

        var methodName;

        for methodName in get_class_methods(this) {
            if "register" !== methodName && substr(methodName, 0, 8) === "register" {
                this->{methodName}();
            }
        }
    }

    /**
     * Passing All Calls To Parent DI
     */
    public function __call(string! methodName, array methodProps = []) -> var | null
    {
        return call_user_func_array([this->getDi(), methodName], methodProps);
    }

    public function set(string! name, args = null, bool! shared = false) -> <Service>
    {
        return this->getDi()->set(name, args, shared);
    }

    public function setShared(string! name, args = null) -> <Service>
    {
        return this->getDi()->setShared(name, args);
    }

    public function get(string! name, array args = null)
    {
        return this->getDi()->get(name, args);
    }

    public function getShared(string! name, array args = null)
    {
        return this->getDi()->get(name, args);
    }

    public function has(string! name)
    {
        return this->getDi()->has(name);
    }

    public function attempt(string! name, var args = null) -> <Service> | bool
    {
        return this->getDi()->attempt(name, args);
    }
}
