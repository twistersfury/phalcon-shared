namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Config\Config;
use Phalcon\Db\Adapter\PdoFactory;

class Database extends AbstractServiceProvider
{
    protected function registerDatabases() -> void
    {
        //Only Load DBs If Configured
        if unlikely !this->{"has"}("config") || !this->{"get"}("config")->has("databases") {
            return;
        }

        var config, name;

        for name, config in this->{"get"}("config")->databases->getIterator() {
            this->registerDatabase(name, <Config> config);
        }
    }

    private function registerDatabase(string! name, <Config> dbConfig)
    {
        var configClone;
        let configClone = <Config> clone dbConfig;

        this->{"setShared"}(
            name,
            function () use (configClone) {
                if likely this->{"has"}("logger") {
                    this->{"get"}("logger")->debug(
                        "TwistersFury\Phalcon\Shared\Di\ServiceProvider\Database",
                        [
                            configClone->toArray()
                        ]
                    );
                }

                return this->{"get"}("Phalcon\\Db\\Adapter\\PdoFactory")->load(configClone);
            }
        );
    }
}
