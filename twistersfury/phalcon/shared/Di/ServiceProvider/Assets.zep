namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Assets\Manager as AssetManager;
use TwistersFury\Phalcon\Shared\Config\Theme;

class Assets extends AbstractServiceProvider
{
    protected function registerAssets() -> void
    {
        this->{"setShared"}(
            "assets",
            function () {
                var assets;

                let assets = <AssetManager> this->{"get"}(
                    "Phalcon\\Assets\\Manager",
                    [
                        this->{"get"}("tag") //Loaded From Phalcon Default
                    ]
                );

                assets->useImplicitOutput(false);

                var themeConfig = <Theme> this->{"get"}("kernel")->getThemeConfig();

                if likely this->{"has"}("logger") {
                    this->{"get"}("logger")->debug(
                        "Assets Theme Name",
                        [
                            "name": themeConfig->getName()
                        ]
                    );
                }

                assets->collection("js")->setSourcePath(
                    themeConfig->getPath()
                )->setAutoVersion(true);

                assets->collection("css")->setSourcePath(
                    themeConfig->getPath()
                )->setAutoVersion(true);

                while themeConfig->hasParent() {
                    let themeConfig = themeConfig->getParent();

                    assets->collection("js")->setSourcePath(
                        themeConfig->getPath()
                    )->setAutoVersion(true);

                    assets->collection("css")->setSourcePath(
                        themeConfig->getPath()
                    )->setAutoVersion(true);
                }

                return assets;
            }
        );
    }
}
