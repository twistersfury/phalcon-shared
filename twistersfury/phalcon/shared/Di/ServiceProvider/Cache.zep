namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Cache\Cache;
use Phalcon\Config\Config;

class Cache extends AbstractServiceProvider
{
    protected function registerCache() -> void
    {
        if unlikely !this->{"get"}("config")->has("cache") {
            return;
        }

        this->setShared(
            "cache",
            function ()
            {
                array adapters = [];
                var adapter, options;

                for adapter in this->{"get"}("config")->cache->adapters->getIterator() {
                    let options = this->{"get"}("config")->cache->get(adapter, new Config())->toArray();
                    if likely this->{"has"}("logger") {
                        this->{"get"}("logger")->debug(
                            "Initializing Cache Adapter",
                            options
                        );
                    }

                    let adapters[] = this->{"get"}("cacheAdapterFactory")->newInstance(
                        adapter,
                        options
                    );
                }

                return this->{"get"}(
                    "TwistersFury\\Phalcon\\Shared\\Cache\\Grouped",
                    [
                        adapters
                    ]
                );
            }
        );
    }

    protected function registerCacheFactory() -> void
    {
        this->set(
            "cacheFactory",
            function () {
                return this->{"get"}(
                    "Phalcon\\Cache\\CacheFactory",
                    [this->{"get"}("cacheAdapterFactory")]
                );
            }
        );
    }

    protected function registerCacheAdapterFactory() -> void
    {
        this->set(
            "cacheAdapterFactory",
            function () {
                return this->{"get"}(
                    "Phalcon\\Cache\\AdapterFactory",
                    [this->{"get"}("seializerFactory")]
                );
            }
        );
    }

    protected function registerSeializerFactory() -> void
    {
        this->set(
            "seializerFactory",
            function () {
                return this->{"get"}(
                    "Phalcon\\Storage\\SerializerFactory"
                );
            }
        );
    }
}
