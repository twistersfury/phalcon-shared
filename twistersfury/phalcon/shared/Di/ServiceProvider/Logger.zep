namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Config\Config;
use Phalcon\Logger\Adapter\AdapterInterface;
use TwistersFury\Phalcon\Shared\Di\ServiceProvider\AbstractServiceProvider;

class Logger extends AbstractServiceProvider
{
    protected function registerLogger() -> void
    {
        var self = this;

        this->{"setShared"}(
            "logger",
            function () use (self) {
                var logger = self->{"get"}("Phalcon\Logger\Logger", [self->{"get"}("config")->logger->name]);
                var adapter = self->{"get"}("config")->logger->defaultAdapter;

                if likely adapter !== null {
                    logger->addAdapter(
                        adapter,
                        self->buildAdapter(adapter)
                    );
                }

                if self->{"get"}("config")->logger->has("adapters") {
                    for adapter in self->{"get"}("config")->logger->get("adapters")->toArray() {
                        logger->addAdapter(
                            adapter,
                            self->buildAdapter(adapter)
                        );
                    }
                }

                logger->setLogLevel(
                    self->{"get"}("config")->logger->logLevel
                );

                return logger;
            }
        );
    }

    public function buildAdapter(string! adapterName) -> <AdapterInterface>
    {
        var config, adapterInstance, formatter;

        let config = <Config> this->{"get"}("config")->logger->{adapterName};
        let adapterInstance = <AdapterInterface> this->{"get"}(adapterName, config->toArray());

        let formatter = <Config> this->{"get"}("config")->logger->formatter;
        if config->has("formatter") {
            let formatter = config->get("formatter");
        }

        if formatter !== null {
            var formatterConfig;

            let formatterConfig = <Config> this->{"get"}("config")->logger->{formatter};

            adapterInstance->setFormatter(
                this->{"get"}(formatter, formatterConfig->toArray())
            );
        }

        return adapterInstance;
    }
}
