namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Config\Config;
use Phalcon\Mvc\Url;

class Url extends AbstractServiceProvider
{
    protected function registerUrl() -> void
    {
        this->{"set"}(
            "url",
            function () {
                var url = <Url> this->{"get"}("Phalcon\\Mvc\\Url"),
                    config = <Config> this->{"get"}("config")->get("system");

                if config->baseUri {
                    url->setBaseUri(config->baseUri);
                }

                if config->staticUri {
                    url->setStaticBaseUri(config->staticUri);
                }

                return url;
            }
        );
    }
}
