namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use TwistersFury\Phalcon\Shared\Config\Interfaces\Theme as ThemeInterface;

class Theme extends AbstractServiceProvider
{
    protected function registerThemeConfig() -> void
    {
        this->setShared(
            "themeConfig",
            function () {
                return this->{"get"}(
                    "TwistersFury\Phalcon\Shared\Config\Interfaces\Theme",
                    [
                        this->{"get"}("kernel")->getThemeName(),
                        this->{"get"}("kernel")->getThemesPath()
                    ]
                );
            }
        );
    }

    protected function registerThemeInterface() -> void
    {
        this->set(
            "TwistersFury\Phalcon\Shared\Config\Interfaces\Theme",
            function (string! themeName, string! basePath) {
                var groupConfig, config, filePath;

                array fileList = [
                    [
                        "adapter": "array",
                        "config": [
                            "cache" : 1440,
                            "scripts": [],
                            "stylesheets":[],
                            "contentTypes":[]
                        ]
                    ]
                ];

                let filePath = basePath . "/config.php";
                if file_exists(filePath) {
                    let fileList[] = filePath;
                }

                let filePath = basePath . "/" . themeName . "/config.php";
                if (file_exists(filePath)) {
                    let fileList[] = filePath;
                }

                let groupConfig = this->{"get"}(
                    "Phalcon\\Config\\Adapter\\Grouped",
                    [
                        fileList
                    ]
                );

                let config = this->{"get"}(
                    "TwistersFury\\Phalcon\\Shared\\Config\\Theme",
                    [[
                        "name": themeName,
                        "basePath": basePath
                    ]]
                );

                config->merge(
                    groupConfig
                );

                if likely this->{"has"}("eventsManager") {
                    this->{"get"}("eventsManager")->fire(
                        "TwistersFury\Phalcon\Shared\Config\Interfaces\Theme:" . themeName,
                        config
                    );
                }

                return config;
            }
        );
    }
}
