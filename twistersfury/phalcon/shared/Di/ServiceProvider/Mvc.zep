namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Assets\Manager as AssetManager;
use Phalcon\Di\DiInterface;
use Phalcon\Forms\Manager as FormsManager;
use Phalcon\Http\Message\ServerRequest;
use Phalcon\Mvc\Router;
use Phalcon\Url;

class Mvc extends View
{
    protected function registerRouter() -> void
    {
        this->{"setShared"}(
            "router",
            function ()
            {
                var router = <Router> this->{"get"}("Phalcon\\Mvc\\Router", [false]);

                router->removeExtraSlashes(true);

                var config = <Config> this->{"get"}("config")->routes;

                router->notFound(
                    config->notFound->toArray()
                );

                router->add(
                    "/",
                    config->defaultRoute->toArray()
                );

                router->setDefaultModule(
                    config->defaults->module
                )->setDefaultController(
                    config->defaults->controller
                )->setDefaultAction(
                    config->defaults->action
                );

                var routeGroup;

                for routeGroup in config->groups->toArray() {
                    router->mount(this->{"get"}(routeGroup));
                }

                return router;
            }
        );
    }

    protected function registerForms() -> void
    {
        this->{"setShared"}(
            "forms",
            function () {
                var forms = new FormsManager();
                var className = "";
                var form = "";

                for form, className in this->{"get"}("config")->forms->toArray() {
                    forms->set(
                        form,
                        this->{"get"}(className)
                    );
                }

                return forms;
            }
        );
    }

    protected function registerServerRequest() -> void
    {
        this->{"setShared"}(
            "serverRequest",
            function () {
                return this->{"get"}("Phalcon\Http\Message\ServerRequest");
            }
        );

        this->{"set"}(
            "Phalcon\Http\Message\ServerRequest",
            function () {
                return this->{"get"}("Phalcon\Http\Message\ServerRequestFactory")->load(
                    _SERVER,
                    _GET,
                    _POST,
                    _COOKIES,
                    _FILES
                );
            }
        );
    }
}
