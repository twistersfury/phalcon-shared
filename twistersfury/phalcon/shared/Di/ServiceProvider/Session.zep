namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Exception;
use Phalcon\Config\Config;
use Phalcon\Di\Service;
use Phalcon\Session\Bag;
use Phalcon\Session\Factory;
use SessionHandlerInterface;
use TwistersFury\Phalcon\Shared\Di\Exception;

class Session extends AbstractServiceProvider
{
    protected function registerSession() -> void
    {
        this->setShared(
            "session",
            function () {
                var config = this->{"get"}("config")->session;

                if !this->{"has"}(config->defaultAdapter) && !class_exists(config->defaultAdapter) {
                    throw new Exception("Session Adapter Doesn't Exist: " . config->defaultAdapter);
                }

                var session = this->{"get"}("Phalcon\\Session\\Manager");

                if likely this->{"has"}("logger") {
                    this->{"get"}("logger")->debug(
                        "Initializing Session",
                        [
                            "adapter" : config->defaultAdapter,
                            "config" : config->get(config->defaultAdapter)->toArray()
                        ]
                    );
                }

                //************************
                //* Adapters Note
                //*************************
                //* Some adapters, like Redis, require options or objects, so the
                //* array method below will not work. To combat this, instead of
                //* using the class name, provide a DIC Service name. Register that
                //* name in the DIC with the parameter being the options. Then return
                //* the adapter from the service.

                var adapter = this->{"get"}(
                    config->defaultAdapter,
                    [
                        config->get(config->defaultAdapter)->toArray()
                    ]
                );

                if !(adapter instanceof SessionHandlerInterface) {
                    throw new Exception("Invalid Adapter");
                }

                session->setAdapter(adapter);

                if unlikely session->start() !== true {
                    throw new Exception("Failed To Start Session");
                }

                return session;
            }
        );
    }

    protected function registerSessionBag() -> void
    {
        var di = this->getDi();

        this->set(
            "sessionBag",
            function (string! name) use (di) {
                return this->{"get"}(
                    "Phalcon\Session\Bag",
                    [
                        this->{"get"}("session"),
                        name
                    ]
                );
            }
        );
    }
}
