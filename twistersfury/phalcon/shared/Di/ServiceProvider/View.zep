namespace TwistersFury\Phalcon\Shared\Di\ServiceProvider;

use Phalcon\Di\DiInterface;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Volt;

class View extends AbstractServiceProvider
{
    protected function registerView() -> void
    {
        this->{"set"}(
            "view",
            function ()
            {
                var view;

                let view = <View> this->{"get"}(
                    "Phalcon\\Mvc\\View",
                    [
                        []
                    ]
                );

                //Note: Theme Logic Was Moved To View Listener

                view->setBasePath(this->{"get"}("kernel")->getThemesPath() . "/")
                    ->setLayoutsDir("layouts/")
                    ->setPartialsDir("partials/");

                view->registerEngines(
                    [
                        ".volt": "Phalcon\\Mvc\\View\\Engine\\Volt"
                    ]
                );

                return view;
            }
        );
    }

    protected function registerVolt() -> void
    {
        this->{"set"}(
            "Phalcon\\Mvc\\View\\Engine\\Volt",
            function (<View> view) {
                bool compileAlways = false;
                if ini_get("tf_shared.debug.mode") {
                    let compileAlways = true;
                }

                var volt = new Volt(view, this);

                volt->setOptions(
                    [
                        "always"     : compileAlways ,
                        "path"      : this->{"get"}("kernel")->getCachePath() . "/",
                        "separator" : "."
                    ]
                );

                var functionNames, functionName;

                let functionNames = get_defined_functions();
                let functionNames = functionNames["internal"];

                for functionName in functionNames {
                    volt->getCompiler()->addFunction(functionName, functionName);
                }

                return volt;
            }
        );
    }

    /**
     * View Specifically Configured For Retrieving E-Mails
     */
    protected function registerViewEmail() -> void
    {
        this->set("viewEmail", function(string! moduleName = "email", string! themeName = null) {
            var view = <View> (clone this->{"get"}("view"));
            var options = [];

            if themeName !== null {
                let options["theme"] = themeName;
            }

            if moduleName !== null {
                let options["module"] = moduleName;
            }

            view->setRenderLevel(View::LEVEL_LAYOUT);

            // Calling `view->toString()` does not fire The beforeRender Method,
            //  so we force call it. Note this will call all beforeRender methods.
            if likely this->{"has"}("eventsManager") {
                this->{"get"}("eventsManager")->fire(
                    "view:beforeRender",
                    view,
                    options
                );
            }

            return view;
        });
    }
}
