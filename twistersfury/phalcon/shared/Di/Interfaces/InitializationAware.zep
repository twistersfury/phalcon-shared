namespace TwistersFury\Phalcon\Shared\Di\Interfaces;

use Phalcon\Di\InitializationAwareInterface;

interface InitializationAware extends InitializationAwareInterface
{
    public function initialize() -> void;
}
