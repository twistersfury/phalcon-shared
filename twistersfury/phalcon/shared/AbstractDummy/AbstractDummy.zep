namespace TwistersFury\Phalcon\Shared\AbstractDummy;

class AbstractDummy
{
    private function doNothing()
    {
        return function() {
            return null;
        };
    }
}
