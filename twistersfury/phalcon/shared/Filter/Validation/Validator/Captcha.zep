namespace TwistersFury\Phalcon\Shared\Filter\Validation\Validator;

use Phalcon\Di\DiInterface;
use Phalcon\Di\InitializationAwareInterface;
use Phalcon\Di\InjectionAwareInterface;
use Phalcon\Filter\Validation;
use Phalcon\Filter\Validation\AbstractValidator;
use RuntimeException;
use TwistersFury\Phalcon\Shared\Traits\Injectable;

class Captcha extends AbstractValidator implements InjectionAwareInterface, InitializationAwareInterface
{
    const API_URI = "https://www.google.com/recaptcha/api/siteverify";

    private apiKey = null;

    private diContainer = null;

    protected template = "Invalid Captcha ";

    public function getDI() -> <DiInterface>
    {
        return this->diContainer;
    }

    public function initialize() -> void
    {
        if unlikely !this->getDi()->has("config") {
            throw new RuntimeException("Missing Application Config");
        }

        if this->getDi()->get("config")->has("google") && this->getDi()->get("config")->google->has("secret") {
            let this->apiKey = this->getDi()->get("config")->google->secret;
        }
    }

    public function setDI(<DiInterface> container) -> void
    {
        let this->diContainer = container;
    }

    public function validate(<Validation> validation, var attribute) -> bool
    {
        // Only Validate If API Supplied (Disabled Otherwise)
        if this->apiKey !== null && !this->verifyCaptcha(
            validation->getValue(attribute),
            this->getDi()->get("request")->getClientAddress()
        ) {
            validation->appendMessage(
                this->messageFactory(
                    validation,
                    attribute
                )
            );

            return false;
        }

        return true;
    }

    protected function verifyCaptcha(string! responseValue, string! remoteIp) -> bool
    {
        array requestParams = [
            "secret"   : this->apiKey,
            "response" : responseValue,
            "remoteip" : remoteIp
        ];

        //TODO: Convert To PSR-7...
        var jsonResponse = json_decode(file_get_contents(Captcha::API_URI . "?" . http_build_query(requestParams)));

        return (bool) jsonResponse->success;
    }
}
