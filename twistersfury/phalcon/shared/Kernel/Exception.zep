namespace TwistersFury\Phalcon\Shared\Kernel;

use TwistersFury\Phalcon\Shared\Exception as SharedException;

class Exception extends SharedException
{
    protected message = "An error occurred while bootstrapping.";
}
