namespace TwistersFury\Phalcon\Shared\Cli;

use TwistersFury\Phalcon\Shared\Kernel as tfKernel;

class Kernel extends tfKernel
{
    protected function getApplicationClass() -> string
    {
        return "Phalcon\Cli\Console";
    }
}
