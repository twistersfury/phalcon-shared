namespace TwistersFury\Phalcon\Shared\Forms;

use Phalcon\Filter\Validation\ValidatorInterface;
use Phalcon\Forms\Element\ElementInterface;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Form;
use Phalcon\Mvc\Model\Resultset;

abstract class AbstractForm extends Form
{
    public function initialize() -> void
    {
        this->setAction(
            this->{"url"}->get(
                [
                    "for": this->{"dispatcher"}->getModuleName() . "-" . this->{"dispatcher"}->getControllerName() . "-save"
                ]
            )
        );
    }

    protected function buildCaptcha(string! elementName = "g-recaptcha-response") -> <Check>
    {
        return this->buildElement(elementName, "TwistersFury\Phalcon\Shared\Forms\Element\Captcha");
    }

    protected function buildCheckbox(string! elementName) -> <Check>
    {
        return this->buildElement(
            elementName,
            "Phalcon\Forms\Element\Check",
            [
                "value": "1"
            ]
        );
    }

    protected function buildElement(string! elementName, string! elementClass, array! elementOptions = []) -> <ElementInterface>
    {
        return this->getDi()->get(
            elementClass,
            [
                elementName,
                elementOptions
            ]
        );
    }

    protected function buildSelect(string! elementName, <Resultset> resultSet, string! displayColumn = "displayName", array! options = []) -> <Select>
    {
        let options = array_merge(
            [
                "emptyText"  : "Select One...",
                "emptyValue" : 0,
                "useEmpty"   : false,
                "using"      : [
                    "id",
                    displayColumn
                ]
            ],
            options
        );

        return this->getDi()->get(
            "Phalcon\Forms\Element\Select",
            [
                elementName,
                resultSet,
                options
            ]
        );
    }

    protected function buildSubmit(string! buttonText, string! elementName = "submit") -> <Submit>
    {
        return this->buildElement(
            elementName,
            "Phalcon\Forms\Element\Submit"
        )->setLabel(buttonText);
    }

    protected function buildText(string! elementName) -> <Text>
    {
        return this->buildElement(elementName, "Phalcon\Forms\Element\Text");
    }

    protected function buildTextarea(string! elementName) -> <Textarea>
    {
        return this->buildElement(elementName, "Phalcon\Forms\Element\TextArea");
    }

    protected function buildValidator(string! className, string! message) -> <ValidatorInterface>
    {
        return this->getDi()->get(
            className,
            [[
                "message": message
            ]]
        );
    }

    protected function buildRequiredValidator(string! message) -> <PresenceOf>
    {
        return this->buildPresenceOfValidator(message);
    }

    protected function buildPresenceOfValidator(string! message) -> <PresenceOf>
    {
        return this->buildValidator(
            "Phalcon\Filter\Validation\Validator\PresenceOf",
            message
        );
    }
}
