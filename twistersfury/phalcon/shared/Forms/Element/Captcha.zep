namespace TwistersFury\Phalcon\Shared\Forms\Element;

use Phalcon\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\InitializationAwareInterface;
use Phalcon\Di\InjectionAwareInterface;
use Phalcon\Forms\Element\AbstractElement;
use RuntimeException;
use TwistersFury\Phalcon\Shared\Filter\Validation\Validator\Captcha as CaptchaValidator;

class Captcha extends AbstractElement implements InjectionAwareInterface, InitializationAwareInterface
{
    protected apiKey = null;

    private diContainer = null;

    public function getDI() -> <DiInterface>
    {
        return this->diContainer;
    }

    public function initialize() -> void
    {
        if unlikely !this->getDi()->has("config") {
            throw new RuntimeException("Missing Application Config");
        }

        if this->getDi()->get("config")->has("google") && this->getDi()->get("config")->google->has("key") {
            let this->apiKey = this->getDi()->get("config")->google->key;
        }

        this->addValidator(
            this->getDi()->get("TwistersFury\Phalcon\Shared\Filter\Validation\Validator\Captcha", [])
        );
    }

    public function setDI(<DiInterface> container) -> void
    {
        let this->diContainer = container;
    }

    public function render(array! attributes = []) -> string
    {
        // Only Return Script If API Key Configured (Disabled Otherwise)
        if this->apiKey === null {
            return "";
        }

        return "<script src=\"https://www.google.com/recaptcha/api.js\"></script><div class=\"g-recaptcha\" data-sitekey=" . this->apiKey . "></div>";
    }
}
