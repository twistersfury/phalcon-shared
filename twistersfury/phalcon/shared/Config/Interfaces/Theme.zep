namespace TwistersFury\Phalcon\Shared\Config\Interfaces;

interface Theme
{
    public function hasParent() -> bool;
    public function getName() -> string;
    public function getParent() -> <Theme>;
    public function getParentName() -> string;
    public function getPath() -> string;
}
