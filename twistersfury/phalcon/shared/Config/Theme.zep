namespace TwistersFury\Phalcon\Shared\Config;

use Phalcon\Config\Adapter\Grouped;
use Phalcon\Config\Config;
use Phalcon\Di\DiInterface;
use Phalcon\Di\InjectionAwareInterface;
use RuntimeException;
use TwistersFury\Phalcon\Shared\Kernel;

class Theme extends Config implements InjectionAwareInterface
{
    private parentConfig = null;

    private dependencyInjector = null;


    public function getBasePath() -> string
    {
        return this->get("basePath");
    }

    /**
     * Returns the internal dependency injector
     */
    public function getDi() -> <DiInterface>
    {
        return this->dependencyInjector;
    }

    public function getName() -> string
    {
        return this->get("name");
    }

    public function getParent() -> <Theme> | null
    {
        if !this->hasParent() {
            return null;
        } elseif this->parentConfig === null {
            let this->parentConfig = this->loadParentConfig();
        }

        return this->parentConfig;
    }

    public function getParentName() -> string | null
    {
        return this->get("parent");
    }

    public function getPath() -> string
    {
        return this->get("basePath") . "/" . this->getName();
    }


    public function hasParent() -> bool
    {
        return this->getParentName() !== null;
    }

    /**
     * Sets the dependency injector
     */
    public function setDi(<DiInterface> container) -> void
    {
        let this->dependencyInjector = container;
    }

    private function loadParentConfig() -> <Theme>
    {
        if unlikely !this->getDi()->has("TwistersFury\Phalcon\Shared\Config\Interfaces\Theme") {
            throw new RuntimeException("Theme ServiceProvider Not Loaded");
        }

        return this->getDi()->get(
            "TwistersFury\Phalcon\Shared\Config\Interfaces\Theme",
            [
                this->getParentName(),
                this->getPath()
            ]
        );
    }
}
