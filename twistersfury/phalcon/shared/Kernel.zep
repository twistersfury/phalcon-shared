namespace TwistersFury\Phalcon\Shared;

use Phalcon\Di\Di;
use Phalcon\Di\DiInterface;
use Phalcon\Di\Injectable;
use Phalcon\Autoload\Loader;
use Phalcon\Application\AbstractApplication;
use TwistersFury\Phalcon\Shared\Config\Theme;
use TwistersFury\Phalcon\Shared\Kernel\Exception;
use TwistersFury\Phalcon\Shared\Di\Http;

class Kernel extends Injectable
{
    private basePath = null;
    private isSetup = false;

    private application;

    public function __construct(<DiInterface> di, string! basePath)
    {
        this->setDi(di);
        Di::setDefault(di);

        this->getDi()->setShared("kernel", this);

        this->setBasePath(basePath);

        if !file_exists(this->getBasePath()) {
            throw this->buildException("Base Path Does Not Exist");
        } elseif !file_exists(this->getApplicationPath()) {
            throw this->buildException("Application Path Does Not Exist");
        } elseif !file_exists(this->getConfigPath()) {
            throw this->buildException("Config Path Does Not Exist");
        }

        this->initialize();

        this->getDi()->get("logger")->debug("kernel Completed");
    }

    protected function initialize() -> <Kernel>
    {
        if likely method_exists(this->getDi(), "initialize") {
            this->getDi()->initialize();
        }

        if !this->getDi()->has("config") {
            throw this->buildException("'config' service not defined in DIC.");
        } elseif !this->getDi()->get("config")->has("modules") {
            throw this->buildException("'modules' configuration missing from Config");
        }



        this->configureEvents()
            ->configureLoader()
            ->loadSession()
            ->loadListeners();

        return this;
    }

    /**
    * Configure the Events Manager for the DIC. Without This, The DI Listener Will Not Work
    */
    protected function configureEvents() -> <Kernel>
    {
        this->getDi()->setInternalEventsManager(this->getDi()->get("eventsManager"));

        return this;
    }

    protected function loadListeners() -> <Kernel>
    {
        if this->getDi()->get("config")->has("listeners") && this->getDi()->has("eventsManager") {
            var listener, instance;

            for listener in this->getDi()->get("config")->listeners->getIterator() {
                let instance = this->getDi()->get("eventsManager")->attach(
                    listener->event,
                    this->getDi()->get(listener->listener)
                );
            }
        }

        return this->registerListeners();
    }

    /**
     * Register Default Listeners
     */
    protected function registerListeners() -> <Kernel>
    {
        // Register Listener For Theme Directories
        this->getDi()->get("eventsManager")->attach(
            "view",
            this->getDi()->get("TwistersFury\\Phalcon\\Shared\\Events\\Listener\\View")
        );

        // Register Listener For Events Manager Injection
        this->getDi()->get("eventsManager")->attach(
            "di",
            this->getDi()->get("TwistersFury\\Phalcon\\Shared\\Events\\Listener\\Di")
        );

        return this;
    }

    protected function loadSession() -> <Kernel>
    {
        if this->getDi()->get("config")->has("session") && this->getDi()->get("config")->session->autoStart {
            this->getDi()->get("session")->start();
        }

        return this;
    }

    public function getBasePath() -> string
    {
        return this->basePath;
    }

    public function setBasePath(string! basePath) -> <Kernel>
    {
        let this->basePath = rtrim(basePath, "/");

        return this;
    }

    public function getApplicationPath() -> string
    {
        return this->getBasePath() . "/app";
    }

    public function getCachePath() -> string
    {
        return this->getStoragePath() . "/cache";
    }

    public function getConfigPath() -> string
    {
        return this->getApplicationPath() . "/config";
    }

    public function getStoragePath() -> string
    {
        return this->getBasePath() . "/storage";
    }

    public function getUploadPath() -> string
    {
        return  this->getStoragePath() . "/uploads";
    }

    public function getLoggerPath() -> string
    {
        return this->getStoragePath() . "/logs";
    }

    public function getApplication() -> <AbstractApplication>
    {
        if !this->application {
            let this->application = this->createApplication();
        }

        return this->application;
    }

    public function getThemesPath() -> string
    {
        return this->getApplicationPath() . "/themes";
    }

    private function createApplication() -> <AbstractApplication>
    {
        var application = this->getDi()->getShared(this->getApplicationClass(), [this->getDi()]);

        application->registerModules(application->getDi()->get("config")->modules->toArray());

        return application;
    }

    protected function getApplicationClass() -> string
    {
        return "Phalcon\\Mvc\\Application";
    }

    private function configureLoader() -> <Kernel>
    {
        array paths = [];

        //Composer, If It Exists
        if file_exists(this->getBasePath() . "/vendor/autoload.php") {
            let paths[] = this->getBasePath() . "/vendor/autoload.php";
        }

        if file_exists(this->getConfigPath() . "/ini_set.php") {
            let paths[] = this->getConfigPath() . "/ini_set.php";
        }

        var loader = this->initLoader();

        loader->setFiles(
            paths
        )->register(true);

        return this;
    }

    private function initLoader() -> <Loader>
    {
        return this->getDi()->get("Phalcon\\Autoload\\Loader");
    }

    private function buildException(string errorMessage) -> <Exception>
    {
        return this->getDi()->get(
            "TwistersFury\\Phalcon\\Shared\\Kernel\\Exception",
            [
                errorMessage
            ]
        );
    }

    public function getThemeName() -> string
    {
        var themeName;

        let themeName = this->getDi()->get("config")->system->theme;

        if this->getDi()->get("session")->theme !== null {
            let themeName = this->getDi()->get("session")->theme->name;
        }

        return themeName;
    }

    public function getThemeConfig() -> <Theme>
    {
        return this->getDi()->get("themeConfig");
    }
}
