namespace TwistersFury\Phalcon\Shared\Support\Mvc\Controller;

use Phalcon\Mvc\Controller;
use Phalcon\Http\Response;
use Phalcon\Mvc\Model;
use DateTime;
use DateInterval;

class AssetsController extends Controller
{
    public function serveAction()
    {
        /** @var Response $response */
        var response = this->buildResponse();
        var theme = this->{"kernel"}->getThemeName();

        // Prepare output path
        var extension = this->{"dispatcher"}->getParam("extension");
        var file = this->{"dispatcher"}->getParam("file");

        string filePath = this->{"kernel"}->getThemesPath() . "/" . theme . "/assets/" . file . "." . extension;
        if (!file_exists(filePath)) {
            return response->setStatusCode(404, "Path Not Found");
        }

        var lastModified;
        let lastModified = new DateTime("@" . filemtime(filePath));
        var fileHash  = md5_file(filePath);

        return this->buildAssetResponse(
            file_get_contents(filePath),
            fileHash,
            lastModified,
            extension
        );
    }

    public function serveModelAction(<Model> model, string photoField = "photo", string updateField = "updatedAt", string extensionField = "extension")
    {
        var fileContents = model->readAttribute(photoField);

        return this->buildAssetResponse(
            fileContents,
            md5(fileContents),
            model->readAttribute(updateField),
            model->readAttribute(extensionField)
        );
    }

    protected function getExpirationInterval() -> string
    {
        return "PT1H";
    }

    protected function buildAssetResponse(string fileContents, string fileHash, <DateTime> dateModified, string extension) -> <Response>
    {
        var response = this->buildResponse();

        var contentTypes = this->getContentTypes();
        var contentType;

        if !fetch contentType, contentTypes[extension] {
            return response->setStatusCode(404, "Mime Not Found");
        }

        var expiresTime;
        let expiresTime = new DateTime();
        expiresTime->add(new DateInterval(this->getExpirationInterval()));

        //Setting Last Modified Date
        response->setContentType(contentType, "UTF-8")
            ->setLastModified(dateModified)
            ->setExpires(expiresTime)
            ->setEtag(fileHash)
            ->setCache(this->getDi()->get("themeConfig")->cache)
            ->setHeader(
                "Pragma",
                "cache"
            );

        //File Hasn't Changed, Don't Request Again
        if (this->{"request"}->getServer("HTTP_IF_NONE_MATCH") === fileHash ||
            this->{"request"}->getServer("HTTP_IF_MODIFIED_SINCE") === dateModified->getTimestamp()
        ) {
            return response->setNotModified();
        }

        // Set the content of the response
        response->setContent(fileContents);

        // Return the response
        return response;
    }

    protected function getContentTypes() -> array
    {
        return array_merge(
            [
                "js" : "application/javascript",
                "css": "text/css",
                "gif": "image/gif",
                "png": "image/png",
                "jpg": "image/jpeg",
                "jpeg":"image/jpeg",
                "jpe":"image/jpeg",
                "map": "application/json",
                "ttf" : "application/octet-stream",
                "woff": "application/octet-stream",
                "woff2": "application/octet-stream",
                "svg": "image/svg+xml"
            ],
            this->getDI()->get("themeConfig")->contentTypes->toArray()
        );
    }

    protected function buildResponse() -> <Response>
    {
        return this->getDI()->get("Phalcon\\Http\\Response");
    }
}
