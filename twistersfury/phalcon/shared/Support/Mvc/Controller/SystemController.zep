namespace TwistersFury\Phalcon\Shared\Support\Mvc\Controller;

use DateTime;
use TwistersFury\Phalcon\Shared\Mvc\Controller\AbstractController;
use Phalcon\Mvc\View;

class SystemController extends AbstractController
{
    public function initialize()
    {
        parent::initialize();

        //Only Render At The Controller Level - Limited Layout
        this->{"view"}->setRenderLevel(View::LEVEL_LAYOUT);
    }

    public function noRouteAction() -> void
    {
        this->{"response"}->setStatusCode(404);
    }

    public function errorAction() -> void
    {
        this->{"response"}->setStatusCode(500);
    }

    public function constructionAction() -> void
    {
        this->{"response"}->setStatusCode(501);
    }

    public function invalidRequestAction() -> void
    {
        this->{"response"}->setStatusCode(400);
    }

    public function expiredAction() -> void
    {
        this->{"response"}->setStatusCode(410);
    }

    public function unAuthenticatedAction() -> void
    {
        this->{"response"}->setStatusCode(401);
    }

    public function unAuthorizedAction() -> void
    {
        this->{"response"}->setStatusCode(403);
    }

    public function maintenanceAction() -> void
    {
        this->{"response"}->setStatusCode(503);
    }
}
