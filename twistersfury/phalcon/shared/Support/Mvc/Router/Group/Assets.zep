namespace TwistersFury\Phalcon\Shared\Support\Mvc\Router\Group;

use Phalcon\Mvc\Router\RouteInterface;
use TwistersFury\Phalcon\Shared\Mvc\Router\AbstractGroup;

class Assets extends AbstractGroup
{
    public function initialize()
    {
        parent::initialize();

        //Overriding Default Prefix
        this->setPrefix("/assets");

        this->addRoute(
            "/(.*)\.([\w]+)",
            [
                "action"     : "serve",
                "file"       : 1,
                "extension"  : 2
            ]
        )->setName("theme-assets");
    }

    protected function getModuleSegment() -> int
    {
        return 3;
    }
}
