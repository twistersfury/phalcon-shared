namespace TwistersFury\Phalcon\Shared\Support\Mvc\Router\Group;

use Phalcon\Mvc\Router\RouteInterface;
use TwistersFury\Phalcon\Shared\Mvc\Router\AbstractGroup;

class System extends AbstractGroup
{
    public function initialize()
    {
        parent::initialize();

        //Overriding Default Prefix
        this->setPrefix("/support/system");

        this->addGet(
            "/error",
            [
                "action"     : "error"
            ]
        )->setName("support-system-error");

        this->addGet(
            "/no-route",
            [
                "action"     : "noroute"
            ]
        )->setName("support-system-no-route");

        this->addGet(
            "/construction",
            [
                "action"     : "construction"
            ]
        )->setName("support-system-construction");

        this->addGet(
            "/invalid",
            [
                "action"     : "invalidrequest"
            ]
        )->setName("support-system-invalid");

        this->addGet(
            "/expired",
            [
                "action"     : "expired"
            ]
        )->setName("support-system-expired");

        this->addGet(
            "/unauthenticated",
            [
                "action"     : "unauthenticated"
            ]
        )->setName("support-system-unauthenticated");

        this->addGet(
            "/unauthorized",
            [
                "action"     : "unauthorized"
            ]
        )->setName("support-system-unauthorized");

        this->addGet(
            "/maintenance",
            [
                "action": "maintenance"
            ]
        );
    }

    protected function getModuleSegment() -> int
    {
        return 3;
    }
}
