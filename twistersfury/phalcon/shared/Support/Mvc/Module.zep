namespace TwistersFury\Phalcon\Shared\Support\Mvc;

use TwistersFury\Phalcon\Shared\Mvc\AbstractModule;

class Module extends AbstractModule
{
    protected function getModuleName() -> string
    {
        return "support";
    }

    protected function getControllerNamespace() -> string
    {
        return "TwistersFury\\Phalcon\\Shared\\Support\\Mvc\\Controller";
    }
}
