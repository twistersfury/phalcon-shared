namespace TwistersFury\Phalcon\Shared\Support;

class Version
{
    public function get() -> string
    {
        return phpversion("tf_shared");
    }
}
