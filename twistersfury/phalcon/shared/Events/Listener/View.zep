namespace TwistersFury\Phalcon\Shared\Events\Listener;

use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Exception;

class View extends Injectable
{
    /**
     * Update The Views Directory Based On Current Settings (Allows Usage In Forwards)
     */
    public function beforeRender(<Event> event, <View> view, var data = null) -> bool
    {
        if !is_array(data) {
            let data = [];
        }

        var themeName, moduleName, groupName;

        if !fetch themeName, data["theme"] {
            let themeName = this->getDi()->get("kernel")->getThemeName();
        }

        if !fetch moduleName, data["module"] {
            let moduleName = this->getDi()->get("dispatcher")->getModuleName();
        }

        if !fetch groupName, data["groupName"] {
            let groupName = this->buildGroupName();
        }

        if !themeName {
            let themeName = "default";
        }

        var viewDirs = [];

        let viewDirs[] = "/" . themeName . "/" . groupName . "/" . moduleName . "/";
        let viewDirs[] = "/" . themeName . "/" . groupName . "/";
        let viewDirs[] = "/" . themeName . "/" . moduleName . "/";
        let viewDirs[] = "/" . themeName . "/";

        if themeName !== "default" {
            let viewDirs[] = "/default/" . groupName . "/" . moduleName . "/";
            let viewDirs[] = "/default/" . groupName . "/";
            let viewDirs[] = "/default/" . moduleName . "/";
            let viewDirs[] = "/default/";
        }

        view->setViewsDir(
            array_values(
                array_unique(
                    preg_replace(
                        "#/+#",
                        "/",
                        viewDirs
                    )
                )
            )
        );

        return true;
    }

    public function notFoundView(<Event> event, <View> view, string! viewEnginePath, var data = null) -> bool
    {
        if unlikely !this->getDi()->has("logger") {
            return true;
        }

        this->{"logger"}->notice(
            "View Not Found",
            [
                "path": view->getActiveRenderPath(),
                "directories": view->getViewsDir(),
                "data": data
            ]
        );

        return true;
    }

    public function buildGroupName() -> string
    {
        var controllerName = this->{"dispatcher"}->getHandlerClass();

        // No Grouping If Not Namespaced Controller
        if !memstr(controllerName, "\\") {
            return "";
        }

        var splitController = explode("\\", controllerName);
        var groupName;

        if !fetch groupName, splitController[this->getGroupSegment()] {
            let groupName = "";
        }

        return strtolower(groupName);
    }

    /**
     * Returns Segment Number of Controller Name to Reference For Group Name.
     *
     * The default is based on a 3-tier approach. Given `TwistersFury\Phalcon\Shared\Support\Controllers\BlahController`,
     *   the group name is `Shared`, resulting in a path of `theme/default/shared/support/blah`
     */
    protected function getGroupSegment() -> int
    {
        return 2;
    }
}
