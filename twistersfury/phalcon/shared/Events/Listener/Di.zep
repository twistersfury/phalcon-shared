namespace TwistersFury\Phalcon\Shared\Events\Listener;

use Phalcon\Di\Di;
use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use Phalcon\Events\EventsAwareInterface;

class Di extends Injectable
{
    /**
     * Inject Events Manager If Events Aware
     */
    public function afterServiceResolve(<Event> event, <Di> diInstance, array data) -> bool
    {
        var instance = data["instance"];

        if is_object(instance) && instance instanceof EventsAwareInterface {
            instance->setEventsManager(diInstance->get("eventsManager"));
        }

        return true;
    }
}
