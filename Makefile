######################################################
#        SYSTEM MAKE FILE                            #
######################################################
# Use this to allow quick/easy modifications and/or  #
# updates to the software. Requires make to be       #
# installed.                                         #
#                                                    #
# Note: This assumes you already have PHP setup and  #
#   properly configured in your path environment     #
######################################################
# Windows                                            #
#                                                    #
# http://gnuwin32.sourceforge.net/downlinks/make.php #
######################################################
# OS X / Linux                                       #
#                                                    #
# Likely Already Installed. Otherwise use the os's   #
# package manager (IE: apt-get, brew, macports, etc) #
######################################################

BIN_PATH := bin

MAKEFILE_DOWNLOAD_COMMAND = wget -O $@
MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

CACHE_PATH := cache

######################################################
# Initial Values (Overrides Included Defaults)
######################################################


APP_DIRECTORY = /opt/tf_shared
DOCKER_ENVS = --env ENV_CODECEPT_LOCAL=1
DOCKER_IMAGE ?= twistersfury/phalcon-shared:83
PHP_IMAGE ?= twistersfury/zephir:83-development
COMPOSER_IMAGE ?= twistersfury/phalcon:83-development
CODECEPT_IMAGE ?= twistersfury/phalcon:83-development
DOCKER_NETWORK = zephir

DEBUG_ENABLE ?= false

APP_DIRECTORY ?= /app

ZEPHIR_EXT_NAME=tf_shared

#ZEPHIR_EXT_NAME
ZEPHIR_IMAGE=twistersfury/zephir:83-development
PHP_IMAGE=$(ZEPHIR_IMAGE)
ZEPHIR_BUILD_SCRIPT=ci/scripts/zephir-build.sh

######################################################
#                 Default Run Command                #
######################################################
.PHONY: default
default: analyse

######################################################
# Include Codecept
######################################################
ifndef MAKEFILE_CODECEPT

include $(BIN_PATH)/codecept.mk

$(BIN_PATH)/codecept.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/master/make/codecept.mk
endif

######################################################
# Include Zephir
######################################################
ifndef MAKEFILE_ZEPHIR

include $(BIN_PATH)/zephir.mk

$(BIN_PATH)/zephir.mk: | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/raw/master/make/zephir.mk
endif

######################################################
#               Hard File Dependencies               #
######################################################

# Primary Cache Directory
$(CACHE_PATH):
	mkdir -p $@

$(BIN_PATH):
	mkdir -p $@

.env: .env.example
	cp .env.example .env

######################################################
#            Installation Dependencies               #
######################################################

.PHONY: install
install: vendor

.PHONY: clean
clean: zephir-full-clean docker-clean
	rm -rf $(BIN_PATH)
	rm -rf $(CACHE_PATH)
	rm -rf vendor
	rm -f compile.log
	rm -f compile-errors.log
	rm -rf .zephir

######################################################
#                    Build Extras                    #
######################################################

.PHONY: docker-build-arm
docker-build-arm: DOCKER_BUILD_ARCH=arm64
docker-build-arm: DOCKER_OPTIONS=--build-arg TAG_VERSION=83 --build-arg RELEASE_VERSION=20230831
docker-build-arm: docker-build-arch


.PHONY: docker-build-amd
docker-build-amd: DOCKER_BUILD_ARCH=amd64
docker-build-amd: DOCKER_OPTIONS=--build-arg TAG_VERSION=83 --build-arg RELEASE_VERSION=20230831
docker-build-amd: docker-build-arch

.PHONY: docker-build-arch
docker-build-arch:
	docker buildx build --memory 1Gb --memory-swap 4Gb --platform linux/$(DOCKER_BUILD_ARCH) --load --pull --build-arg TAG_VERSION=83 --build-arg RELEASE_VERSION=20230831  --target development -t $(DOCKER_IMAGE)-development  --secret id=composer,src=$$HOME/.composer/auth.json -f docker/zephir/Dockerfile . $(DOCKER_OPTIONS)
	docker buildx build --memory 1Gb --memory-swap 4Gb --platform linux/$(DOCKER_BUILD_ARCH) --load --pull --build-arg TAG_VERSION=83 --build-arg RELEASE_VERSION=20230831  -t $(DOCKER_IMAGE)  --secret id=composer,src=$$HOME/.composer/auth.json -f docker/zephir/Dockerfile . $(DOCKER_OPTIONS)

#	docker pull registry.gitlab.com/twistersfury/phalcon-shared/phalcon-shared:dev-83-development-development
#	docker tag registry.gitlab.com/twistersfury/phalcon-shared/phalcon-shared:dev-83-development-development twistersfury/phalcon-shared:83-development
#	docker push twistersfury/phalcon-shared:83-development

.PHONY: docker-build-local
docker-build-local: docker-build-arm docker-build-mount

.PHONY: docker-build-mount
docker-build-mount:
	docker run --rm -it -v "$(CURDIR)":/opt/tf_shared  -w "//opt/tf_shared" $(DOCKER_IMAGE)-development php -d memory_limit=-1 /composer/vendor/bin/zephir clean
	docker run --rm -it -v "$(CURDIR)":/opt/tf_shared  -w "//opt/tf_shared" $(DOCKER_IMAGE)-development php -d memory_limit=-1 /composer/vendor/bin/zephir fullclean
	docker run --rm -it -v "$(CURDIR)":/opt/tf_shared  -w "//opt/tf_shared" $(DOCKER_IMAGE)-development php -d memory_limit=-1 /composer/vendor/bin/zephir generate --export-classes
	docker run --rm -it -v "$(CURDIR)":/opt/tf_shared  -w "//opt/tf_shared" $(DOCKER_IMAGE)-development php -d memory_limit=-1 /composer/vendor/bin/zephir compile --export-classes --dev
