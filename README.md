# Introduction

This project exists as a template for a Phalcon Based Zephir Project. The template comes pre-installed with Phalcon, Zephir, and Codeception. It also comes with a pre-built Makefile to make usage easier.

## Initializing Template - READ THIS FIRST

Make sure to read this document before running the commands. Otherwise you project might break on init. In order to init the template, run the command:

```shell
make init-template
```

Note that after you run this command, the `init-template` command will be removed from the Makefile.

## Template Prompt
When you run the initialization command, it will prompt you for several answers. These answers will be used in initalizing the plugin. Please see below as to what the questions are and what there answers should look like. These answers will be used to adjust the entire project.

1. `Extension Name` - This should be lowercase, with only underscores. This will become you final file name. IE: `blah` will generate `blah.so`.
1. `Namespace` - This is the namespace used for both the extension folder as well as the PHP Namespace. If you wish to use subfolders (IE: `My\Name\Space`), be sure to escape the \ in your answer. IE: `My\\Name\\Space`
1. `Description` - A description of your extension. Note that this will be put into your composer.json file, so you need to not use double quotes (`"`) or escape them if you do.
1. `Author` - Your Name.
1. `E-Mail` - Your E-Mail Address.

## Debugging/Testing

docker-compose.override.yml:
```yaml
version: "3.5"
services:
  nginx:
    ports:
      - 8080:80/tcp
  php:
    build:
      args:
        ZEPHIR_DEBUG: 1

```
